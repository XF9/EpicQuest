﻿#define VS_SHADERMODEL vs_4_0_level_9_3
#define PS_SHADERMODEL ps_4_0_level_9_3

Texture2D SpriteTexture;

const static int MAX_KERNEL_STEP_COUNT = 15;
float2 KERNEL_STEPS[MAX_KERNEL_STEP_COUNT];
float KERNEL_WEIGHT[MAX_KERNEL_STEP_COUNT];
float AMPLIFIER;
bool USE_CUSTOM_COLOR;
float4 CUSTOM_COLOR;

sampler2D SpriteTextureSampler = sampler_state
{
	Texture = <SpriteTexture>;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR0;
	float2 TextureCoordinates : TEXCOORD0;
};

float4 Blur(VertexShaderOutput input) : COLOR
{
    float2 coords = input.TextureCoordinates;
    float4 color = float4(0, 0, 0, 0);

    for (int i = 0; i < MAX_KERNEL_STEP_COUNT; i++)
        color += tex2D(SpriteTextureSampler, float2(coords + KERNEL_STEPS[i])) * KERNEL_WEIGHT[i];

    if (USE_CUSTOM_COLOR)
        return float4(CUSTOM_COLOR.rgb, color.a * AMPLIFIER);
    else
        return float4(color.rgb, color.a * AMPLIFIER);
}

technique SpriteDrawing
{
    pass Blur
    {
        PixelShader = compile PS_SHADERMODEL Blur();
    }
};