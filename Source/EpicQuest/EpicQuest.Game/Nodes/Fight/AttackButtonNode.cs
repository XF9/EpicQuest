﻿using EpicQuest.Engine;
using EpicQuest.Engine.SceneTree;
using EpicQuest.Game.Model.Attacks;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Game.Nodes.Fight
{
    class AttackButtonNode : ButtonBaseNode<TurretNode>
    {
        public AttackButtonNode(TurretNode turret)
        {
            SetSize(40, 40);
            SetBackground(GameManager.ContentManager.GetAsset<Texture2D>(turret.Attack.Icon), turret.Attack.Color * .6f);
            SetActiveBackground(GameManager.ContentManager.GetAsset<Texture2D>(turret.Attack.Icon), turret.Attack.Color);
            Payload = turret;
        }
    }
}
