﻿using System;
using System.Collections.Generic;
using EpicQuest.Engine;
using EpicQuest.Engine.EventArgs;
using EpicQuest.Engine.SceneTree;
using EpicQuest.Game.Helper;
using EpicQuest.Game.Model;
using EpicQuest.Game.Model.Attacks;
using EpicQuest.Game.Nodes.Ui;
using EpicQuest.Game.Scenes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Game.Nodes.Fight
{
    class InfoPanelNode : SceneTreeNode
    {
        private readonly SpriteFont _fontHeader;
        private readonly SpriteFont _fontText;
        private const int Height = 200;
        private const int VerticalElementSpacing = 15;
        private const int HorizontalElementSpacing = 10;

        private Unit _attackingUnit;

        private readonly TableNode _attackingUnitInfo;
        private readonly List<AttackButtonNode> _attackButtons;

        public EventHandler<PayloadEventArgs<TurretNode>> OnAttackSelected;
        public EventHandler<PayloadEventArgs<TurretNode>> OnAttackHoverStart;
        public EventHandler<PayloadEventArgs<TurretNode>> OnAttackHoverEnd;

        public InfoPanelNode(Texture2D backGroundTexture, SpriteFont fontHeader, SpriteFont fontText)
        {
            var screenSize = GameManager.GetInstance().Screen.InternalSize;
            _fontHeader = fontHeader;
            _fontText = fontText;
            SetSize(screenSize.X, Height);
            _attackButtons = new List<AttackButtonNode>();

            var background = new TextureNode(backGroundTexture, Color.White * .3f);
            background.SetSize(screenSize.X, Height);
            background.SetBorder(1, Content.FightScene.BorderColor, TextureNode.Border.Top);
            AddChild(background);

            var buttoncancel = new MainMenuButtonNode(Resources.Strings.Fight_BackToMainMenu);
            buttoncancel.SetPosition(
                GetSize().X - buttoncancel.GetSize().X - HorizontalElementSpacing,
                Height - buttoncancel.GetSize() .Y - VerticalElementSpacing);
            buttoncancel.OnClick += (sender, args) => GameManager.GetInstance().RequestScene(nameof(MainMenuScene));
            AddChild(buttoncancel);

            _attackingUnitInfo = new TableNode();
            _attackingUnitInfo.SetPosition(HorizontalElementSpacing, VerticalElementSpacing);
            AddChild(_attackingUnitInfo);

            SetPosition(0, screenSize.Y - Height);
        }

        public void SetAttackingUnit(Unit attackingUnit)
        {
            ClearAttackingUnit();

            if (attackingUnit == null)
                return;

            _attackingUnit = attackingUnit;

            _attackingUnitInfo.AddRows(new List<TableNode.TableRow>
            {
                new TableNode.TableRow
                {
                    IsHeader = true,
                    Color = Content.Colors.DefaultTextColorHeading,
                    Font = _fontHeader,
                    Data = new List<string>{ attackingUnit.Name }
                },
                new TableNode.TableRow
                {
                    Color = Content.Colors.DefaultTextColor,
                    Font = _fontText,
                    Data = new List<string>{ Resources.Strings.Hitpoints, $"{attackingUnit.Hitpoints} ({attackingUnit.MaximumHitpoints})" }
                },
                new TableNode.TableRow
                {
                    Color = Content.Colors.DefaultTextColor,
                    Font = _fontText,
                    Data = new List<string>{ Resources.Strings.Shield, $"{attackingUnit.Shield} ({attackingUnit.MaximumShield})" }
                },
            });

            for(int index = 0; index < attackingUnit.Turrets.Count; index++)
            {
                var turret = attackingUnit.Turrets[index];
                var node = new AttackButtonNode(turret);

                int x = HorizontalElementSpacing + _attackingUnitInfo.GetSize().X + HorizontalElementSpacing + index * (node.GetSize().X + HorizontalElementSpacing);
                int y = VerticalElementSpacing;

                node.SetPosition(x, y);
                node.OnClick += (sender, args) => OnAttackSelected?.Invoke(this, args);
                node.OnMouseEnter += (sender, args) => OnAttackHoverStart?.Invoke(this, args);
                node.OnMouseLeft += (sender, args) => OnAttackHoverEnd?.Invoke(this, args);

                AddChild(node);
                _attackButtons.Add(node);
            }
        }

        private void ClearAttackingUnit()
        {
            _attackingUnit = null;
            _attackingUnitInfo.RemoveRows();

            foreach (var attackButton in _attackButtons)
                RemoveChild(attackButton);
            _attackButtons.Clear();
        }
    }
}
