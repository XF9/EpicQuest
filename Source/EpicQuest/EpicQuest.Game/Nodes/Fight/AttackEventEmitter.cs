﻿using System.Collections.Generic;
using System.Linq;
using EpicQuest.Engine;
using EpicQuest.Engine.SceneTree;
using EpicQuest.Engine.State;
using EpicQuest.Game.Helper;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Game.Nodes.Fight
{
    class AttackEventEmitter : SceneTreeNode
    {
        private readonly List<UnitEventTextNode> _attackEventQueue = new List<UnitEventTextNode>();
        public int EventCooldown { get; set; } = 1000;
        private int _eventCooldownTimer = 0;

        protected override void Update(IGameState state)
        {
            if (_eventCooldownTimer > 0)
                _eventCooldownTimer -= state.GameTime.ElapsedGameTime.Milliseconds;
            else if (_attackEventQueue.Any())
            {
                Emitt(_attackEventQueue.First());
                _attackEventQueue.RemoveAt(0);
                _eventCooldownTimer = EventCooldown;
            }
        }

        private void Emitt(UnitEventTextNode node)
        {
            AddChild(node);
            node.Move(0, -60, 2000, () => RemoveChild(node));
        }

        public void ShowTextEvent(string text, Color color)
        {
            var node = new UnitEventTextNode(GameManager.ContentManager.GetAsset<SpriteFont>(Content.Fonts.DefaultFontLarge), text);
            node.SetColor(color);
            node.SetPosition(0, 0);
            node.SetSize(GetSize().X, node.GetSize().Y);
            _attackEventQueue.Add(node);
        }
    }
}
