﻿using EpicQuest.Engine.SceneTree;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Game.Nodes.Fight
{
    class UnitEventTextNode : TextNode
    {
        public UnitEventTextNode(SpriteFont font, string text) : base(font, text)
        {
        }
    }
}
