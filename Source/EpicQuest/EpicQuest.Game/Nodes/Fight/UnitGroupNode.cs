﻿using EpicQuest.Engine.SceneTree;
using EpicQuest.Game.Model;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using System;
using System.Linq;
using EpicQuest.Engine.EventArgs;
using EpicQuest.Game.Enums;

namespace EpicQuest.Game.Nodes.Fight
{
    class UnitGroupNode : SceneTreeNode
    {
        private readonly Dictionary<Point, UnitNode> _unitNodes;

        private const int ColumnCount = 2;
        private const int RowCount = 3;

        public event EventHandler<PayloadEventArgs<UnitNode>> UnitNodeSelected;
        public event EventHandler<PayloadEventArgs<UnitNode>> UnitNodeHoverStart;
        public event EventHandler<PayloadEventArgs<UnitNode>> UnitNodeHoverEnd;

        private readonly Diplomacy _diplomacy;

        public UnitGroupNode(Diplomacy diplomacy)
        {
            _diplomacy = diplomacy;
            _unitNodes = new Dictionary<Point, UnitNode>();

            var size = new Point(ColumnCount * UnitNode.Width, RowCount * UnitNode.Height);

            SetSize(size);
        }

        public void AddUnit(Point position, Unit unit)
        {
            unit.Diplomacy = _diplomacy;

            var node = new UnitNode(unit, _diplomacy);
            node.UnitSelected += (sender, args) => UnitNodeSelected?.Invoke(this, args);
            node.UnitHoverStart += (sender, args) => UnitNodeHoverStart?.Invoke(this, args);
            node.UnitHoverEnd += (sender, args) => UnitNodeHoverEnd?.Invoke(this, args);

            Point unitPosition;
            if (_diplomacy == Diplomacy.Friend)
            {
                unitPosition = new Point(UnitNode.Width * position.X, position.Y * UnitNode.Height);
            }
            else
            {
                var column = ColumnCount - 1 - position.X;
                unitPosition = new Point(UnitNode.Width * column, position.Y * UnitNode.Height);
            }

            node.SetPosition(unitPosition.X, unitPosition.Y);

            AddChild(node);
            _unitNodes.Add(position, node);
        }

        public IEnumerable<UnitNode> GetUnits()
        {
            return _unitNodes.Values.ToList();
        }

        public IEnumerable<UnitNode> GetLivingUnits()
        {
            return GetUnits().Where(u => !u.Unit.IsDead());
        }
    }
}
