﻿using System;
using EpicQuest.Engine;
using EpicQuest.Engine.SceneTree;
using EpicQuest.Engine.State;
using EpicQuest.Game.Helper;
using EpicQuest.Game.Model;
using Microsoft.Xna.Framework;
using EpicQuest.Engine.EventArgs;
using EpicQuest.Game.Enums;
using EpicQuest.Game.Model.Attacks;
using EpicQuest.Game.Shader;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Game.Nodes.Fight
{
    public class UnitNode : SceneTreeNode
    {
        public Unit Unit { get; }
        private readonly TextureNode _unitIcon;
        private readonly AttackEventEmitter _attackEventEmitter;

        private bool _unitWasAlive = true;

        public bool Selectable { get; set; }
        public bool Selected { get; set; }
        public event EventHandler<PayloadEventArgs<UnitNode>> UnitSelected;
        public event EventHandler<PayloadEventArgs<UnitNode>> UnitHoverStart;
        public event EventHandler<PayloadEventArgs<UnitNode>> UnitHoverEnd;

        private readonly GlowShader _glowShader;
        private readonly ButtonNode _clicker;

        private readonly Diplomacy _diplomacy;

        public const int Width = 360;
        public const int Height = 250;

        public Vector3 UnitCenter => new Vector3(180, 100, 0);

        public UnitNode(Unit unit, Diplomacy diplomacy)
        {
            Unit = unit;
            _diplomacy = diplomacy;
            SetSize(Width, Height);

            _clicker = new ButtonNode();
            _clicker.SetBackground(GameManager.ContentManager.GetAsset<Texture2D>(Content.Defaults.BlankTexture), Color.White * 0);
            _clicker.OnClick += (sender, args) => OnClick();
            _clicker.OnMouseEnter += (sender, aargs) => UnitHoverStart?.Invoke(this, new PayloadEventArgs<UnitNode>(this));
            _clicker.OnMouseLeft += (sender, aargs) => UnitHoverEnd?.Invoke(this, new PayloadEventArgs<UnitNode>(this));

            var unitAncor = new EmptyNode();
            unitAncor.SetPosition(UnitCenter.X, UnitCenter.Y);
            unitAncor.RotateTo(diplomacy == Diplomacy.Enemy ? 180 : 0);

            var icon = unit.GetUnitImage();
            _unitIcon = new TextureNode(GameManager.ContentManager.GetAsset<Texture2D>(icon));
            _unitIcon.SetPosition(-_unitIcon.GetSize().X / 2f, -_unitIcon.GetSize().Y / 2f, 0, () =>
            {
                var unitIconOffset = unitAncor.Transformation.Translation + _unitIcon.Transformation.Translation;
                _clicker.SetPosition(unitIconOffset.X, unitIconOffset.Y);
                _clicker.SetSize(_unitIcon.GetSize().X, _unitIcon.GetSize().Y);
            });

            foreach (var unitTurret in Unit.Turrets)
                _unitIcon.AddChild(unitTurret);

            var healthbar = new HealthbarNode(unit);
            healthbar.SetPosition((Width - healthbar.GetSize().X) / 2, 220);

            var shieldbar = new ShieldbarNode(unit);
            shieldbar.SetPosition((Width - shieldbar.GetSize().X) / 2, 240);

            _attackEventEmitter = new AttackEventEmitter();

            _glowShader = new GlowShader(GameManager.ContentManager.GetAsset<Effect>(Content.Shader.Glow), GameManager.ContentManager.GetAsset<Texture2D>(unit.GetUnitImage()));

            unitAncor.AddChild(_unitIcon);
            AddChild(_clicker);
            AddChild(_attackEventEmitter);
            AddChild(unitAncor);
            AddChild(healthbar);
            AddChild(shieldbar);
        }

        protected override void Update(IGameState state)
        {
            if (_unitWasAlive && Unit.IsDead())
            {
                _unitWasAlive = false;
                _unitIcon.Color = Content.FightScene.DeadUnit;
            }

            if (Selectable && Selected)
            {
                _unitIcon.Shader = _glowShader;
                _glowShader.Color = _diplomacy == Diplomacy.Friend ? Content.FightScene.SelectedUnitFriend : Content.FightScene.SelectedUnitEnemy;
            }
            else if (Selectable)
            {
                _unitIcon.Shader = _glowShader;
                _glowShader.Color = _clicker.IsActive
                    ? _diplomacy == Diplomacy.Friend
                        ? Content.FightScene.SelectableUnitFriendHovered
                        : Content.FightScene.SelectableUnitEnemyHovered
                    : _diplomacy == Diplomacy.Friend
                        ? Content.FightScene.SelectableUnitFriend
                        : Content.FightScene.SelectableUnitEnemy;
            }
            else
            {
                _unitIcon.Shader = null;
            }
        }

        public void ApplyCombatResult(AttackResultDo result)
        {
            if(result.Missed)
                _attackEventEmitter.ShowTextEvent(Resources.Strings.Attack_Missed, Content.FightScene.AttackFailed);
            else if(result.Evaded)
                _attackEventEmitter.ShowTextEvent(Resources.Strings.Attack_Evaded, Content.FightScene.AttackFailed);
            else if (result.Critical)
            {
                _attackEventEmitter.ShowTextEvent(String.Format(Resources.Strings.Attack_Critical, result.Damage), Content.FightScene.AttackCritical);
                _attackEventEmitter.ShowTextEvent(String.Format(Resources.Strings.Attack_Successfull, result.Damage), Content.FightScene.AttackCritical);
                Unit.TakeDamage(result.Damage);
            }
            else
            {
                _attackEventEmitter.ShowTextEvent(String.Format(Resources.Strings.Attack_Successfull, result.Damage), Content.FightScene.AttackSuccessfull);
                Unit.TakeDamage(result.Damage);
            }
        }

        private void OnClick()
        {
            if(Selectable)
                UnitSelected?.Invoke(this, new PayloadEventArgs<UnitNode>(this));
        }
    }
}
