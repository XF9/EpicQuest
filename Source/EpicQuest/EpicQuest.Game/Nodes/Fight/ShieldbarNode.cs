﻿using EpicQuest.Engine.State;
using EpicQuest.Game.Helper;
using EpicQuest.Game.Model;

namespace EpicQuest.Game.Nodes.Fight
{
    class ShieldbarNode : BarNode
    {
        private readonly Unit _unit;

        public ShieldbarNode(Unit unit) : base(unit.MaximumShield)
        {
            _unit = unit;
            ForegroundColor = Content.FightScene.ShieldbarForeground;
        }

        protected override void Update(IGameState state)
        {
            Value = _unit.Shield;
        }
    }
}
