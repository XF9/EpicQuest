﻿using EpicQuest.Engine;
using EpicQuest.Engine.SceneTree;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Game.Nodes.Fight
{
    abstract class BarNode : SceneTreeNode
    {
        protected readonly TextureNode BarForegroundNode;
        protected readonly TextureNode BarBackgroundNode;

        private const int InitialWidth = 200;
        private const int InitialHeigth = 10;
        private readonly int MaximumValue;
        public int Value
        {
            set
            {
                if(value != MaximumValue)
                {
                    int width = value * 100 / MaximumValue * GetSize().X / 100;
                    BarForegroundNode.SetSize(width, GetSize().Y);
                }
            }
        }

        private Color _foregroundColor;
        public Color ForegroundColor
        {
            get => _foregroundColor;
            set
            {
                _foregroundColor = value;
                BarForegroundNode.Color = _foregroundColor;
            }
        }
        private Color _backgroundColor;
        public Color BackgroundColor
        {
            get => _backgroundColor;
            set
            {
                _backgroundColor = value;
                BarBackgroundNode.Color = _backgroundColor;
            }
        }

        public BarNode(int maximalValue)
        {
            MaximumValue = maximalValue;
            SetSize(InitialWidth, InitialHeigth);

            BarForegroundNode = new TextureNode(GameManager.ContentManager.GetAsset<Texture2D>(DefaultContent.Defaults.BlankTexture), InitialWidth, InitialHeigth, Color.Black);
            AddChild(BarForegroundNode, 1);

            BarBackgroundNode = new TextureNode(GameManager.ContentManager.GetAsset<Texture2D>(DefaultContent.Defaults.BlankTexture), InitialWidth, InitialHeigth, Color.Black);
            AddChild(BarBackgroundNode, 0);
        }
    }
}
