﻿using System.Collections.Generic;
using EpicQuest.Game.Enums;
using EpicQuest.Game.Helper;
using EpicQuest.Game.Model;
using EpicQuest.Game.Model.Attacks;
using EpicQuest.Game.Nodes.Ui;

namespace EpicQuest.Game.Nodes.Fight
{
    class FightHoverPanel : HoverPanel
    {
        public FightHoverPanel()
        {
            Background.SetBorder(1, Content.FightScene.BorderColor);
        }

        public void DisplayUnitData(Unit unit)
        {
            RemoveContent();
            SetContent(new List<TableNode.TableRow>
            {
                new TableNode.TableRow
                {
                    IsHeader = true,
                    Color = unit.Diplomacy == Diplomacy.Friend ? Content.FightScene.UnitFriend : Content.FightScene.UnitEnemy,
                    Data = new List<string>{ unit.Name }
                },
                new TableNode.TableRow
                {
                    Data = new List<string>{Resources.Strings.Hitpoints, $"{unit.Hitpoints} ({unit.MaximumHitpoints})"}
                },
                new TableNode.TableRow
                {
                    Data = new List<string>{Resources.Strings.Shield, $"{unit.Shield} ({unit.MaximumShield})"}
                },
            });

            if(unit.Diplomacy == Diplomacy.Friend)
                Background.SetBorderColor(Content.FightScene.UnitFriend);
            else
                Background.SetBorderColor(Content.FightScene.UnitEnemy);
        }

        public void DisplayAttackData(Attack attack)
        {
            RemoveContent();
            SetContent(new List<TableNode.TableRow>
            {
                new TableNode.TableRow
                {
                    IsHeader = true,
                    Data = new List<string>{ attack.Name }
                },
                new TableNode.TableRow
                {
                    Data = new List<string>{Resources.Strings.Attack_Damage, $"{attack.MinDamage} - {attack.MaxDamage}"}
                },
                new TableNode.TableRow
                {
                    Data = new List<string>{Resources.Strings.Attack_HitChance, $"{attack.HitChance:n2}%"}
                },
                new TableNode.TableRow
                {
                    Data = new List<string>{Resources.Strings.Attack_CrtiChance, $"{attack.CritChance:n2}%"}
                },
                new TableNode.TableRow
                {
                    Data = new List<string>{Resources.Strings.Attack_CritModifier, $"{(attack.CritModifier * 100):n2}%"}
                },
            });

            Background.SetBorderColor(Content.FightScene.BorderColor);
        }

        public void DisplayAttackResultData(Attack attack, Unit defender)
        {
            var stats = CombatCalculator.CalculateAttackStatistics(attack, defender);
            RemoveContent();

            SetContent(new List<TableNode.TableRow>
            {
                new TableNode.TableRow
                {
                    IsHeader = true,
                    Color = defender.Diplomacy == Diplomacy.Friend ? Content.FightScene.UnitFriend : Content.FightScene.UnitEnemy,
                    Data = new List<string>{ defender.Name }
                },
                new TableNode.TableRow
                {
                    Data = new List<string>{Resources.Strings.Hitpoints, $"{defender.Hitpoints} ({defender.MaximumHitpoints})"}
                },
                new TableNode.TableRow
                {
                    Data = new List<string>{Resources.Strings.Shield, $"{defender.Shield} ({defender.MaximumShield})"}
                },
                new TableNode.TableRow
                {
                    Data = new List<string>{Resources.Strings.Attack_HitChance, $"{stats.HitChance:n2}%"}
                },
                new TableNode.TableRow
                {
                    Data = new List<string>{Resources.Strings.Attack_Damage, $"{attack.MinDamage} - {attack.MaxDamage}"}
                },
            });

            Background.SetBorderColor(Content.FightScene.UnitEnemy);
        }
    }
}
