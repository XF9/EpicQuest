﻿using EpicQuest.Engine.State;
using EpicQuest.Game.Helper;
using EpicQuest.Game.Model;

namespace EpicQuest.Game.Nodes.Fight
{
    class HealthbarNode : BarNode
    {
        private readonly Unit _unit;

        public HealthbarNode(Unit unit) : base(unit.MaximumHitpoints)
        {
            _unit = unit;
            ForegroundColor = Content.FightScene.HealthbarForeground;
        }

        protected override void Update(IGameState state)
        {
            Value = _unit.Hitpoints;
        }
    }
}
