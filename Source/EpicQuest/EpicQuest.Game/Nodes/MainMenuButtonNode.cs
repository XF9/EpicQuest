﻿using EpicQuest.Engine;
using EpicQuest.Engine.SceneTree;
using EpicQuest.Game.Helper;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Game.Nodes
{
    class MainMenuButtonNode : TextButtonNode
    {
        public MainMenuButtonNode(string text) : base(text, GameManager.ContentManager.GetAsset<SpriteFont>(Content.Fonts.DefaultFontLarge), Content.Colors.ButtonTextColor)
        {
            SetSize(350, 50);
            SetBackground(GameManager.ContentManager.GetAsset<Texture2D>(DefaultContent.Defaults.BlankTexture), Content.Colors.ButtonColor);
            SetActiveBackground(GameManager.ContentManager.GetAsset<Texture2D>(DefaultContent.Defaults.BlankTexture), Content.Colors.ButtonHoverColor);
        }
    }
}
