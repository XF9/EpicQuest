﻿using System;
using System.Collections.Generic;
using System.Linq;
using EpicQuest.Engine.SceneTree;
using EpicQuest.Engine.State;
using EpicQuest.Game.Helper;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Game.Nodes.OptionNodes
{
    class LeftRightSelectorNode<T> : SceneTreeNode
    {
        private readonly KeyValuePair<T, string>[] _values;
        private int _index;

        private readonly TextButtonNode _valueLabel;
        private readonly SpriteFont _font;

        public LeftRightSelectorNode(KeyValuePair<T, string>[] values, SpriteFont font, Texture2D buttonTexture, Texture2D buttonTextureHover)
        {
            _values = values;
            _index = 0;
            _font = font;

            var handleSize = new Point(50, 50);
            var spacing = new Point(10, 0);
            var labelSize = new Point(350, 50);

            var leftHandle = new TextButtonNode("<", _font, Content.Colors.ButtonTextColor);
            leftHandle.SetPosition(0, 0);
            leftHandle.SetSize(handleSize);
            leftHandle.SetBackground(buttonTexture, Content.Colors.ButtonColor);
            leftHandle.SetActiveBackground(buttonTextureHover, Content.Colors.ButtonHoverColor);

            var rightHandle = new TextButtonNode(">", _font, Content.Colors.ButtonTextColor);
            rightHandle.SetPosition(handleSize.X + labelSize.X + 2 * spacing.X, 0);
            rightHandle.SetSize(handleSize);
            rightHandle.SetBackground(buttonTexture, Content.Colors.ButtonColor);
            rightHandle.SetActiveBackground(buttonTextureHover, Content.Colors.ButtonHoverColor);

            _valueLabel = new TextButtonNode(String.Empty, _font, Content.Colors.ButtonTextColor);
            _valueLabel.SetPosition(handleSize.X + spacing.X, 0);
            _valueLabel.SetSize(labelSize);
            _valueLabel.SetBackground(buttonTexture, Content.Colors.ButtonColor);

            AddChild(leftHandle);
            AddChild(_valueLabel);
            AddChild(rightHandle);

            leftHandle.OnClick += (sender, args) => _index = (_index + _values.Length - 1) % _values.Length;
            rightHandle.OnClick += (sender, args) => _index = (_index + 1) % _values.Length;
        }

        public T GetCurrentValue()
        {
            return _values[_index].Key;
        }

        public void SetValue(T value)
        {
            if (!_values.Any(o => o.Key.Equals(value)))
                return;

            var element = _values.First(o => o.Key.Equals(value));
            _index = Array.IndexOf(_values, element);
        }

        protected override void Update(IGameState state)
        {
            _valueLabel.SetText(_values[_index].Value, _font, Content.Colors.ButtonTextColor);
        }
    }
}
