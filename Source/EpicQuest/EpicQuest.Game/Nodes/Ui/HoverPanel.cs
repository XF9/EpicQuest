﻿using System.Collections.Generic;
using EpicQuest.Engine;
using EpicQuest.Engine.SceneTree;
using EpicQuest.Engine.State;
using EpicQuest.Game.Helper;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Game.Nodes.Ui
{
    class HoverPanel : SceneTreeNode
    {
        protected readonly TextureNode Background;

		private readonly Point _offset = new Point(40, 40);
        private readonly int _contentPadding = 10;
        private readonly TableNode _data;

        public HoverPanel()
        {
            SetVisibility(true);
			Background = new TextureNode(GameManager.ContentManager.GetAsset<Texture2D>(Content.Defaults.BlankTexture), 0, 0);
            Background.Color = Content.FightScene.HoverPanelBackgroundColor;
            _data = new TableNode();
            _data.SetPosition(_contentPadding, _contentPadding);
            AddChild(Background);
            AddChild(_data);
        }

        protected void SetContent(IEnumerable<TableNode.TableRow> data)
        {
            _data.RemoveRows();
            _data.AddRows(data);
            UpdateSize();
        }

        protected void RemoveContent()
        {
            _data.RemoveRows();
            UpdateSize();
        }

        private void UpdateSize()
        {
            var dataSize = _data.GetSize();
            var size = dataSize != Point.Zero ? dataSize + new Point(_contentPadding * 2, _contentPadding * 2) : Point.Zero;
            Background.SetSize(size);
            SetSize(size);
        }

        protected override void Update(IGameState state)
        {
            var screensize = GameManager.GetInstance().Screen.InternalSize;
            var mousposition = state.MouseState.Position;
            var size = GetSize();

            var maxX = screensize.X - size.X;
            var x = mousposition.X + _offset.X;

            var maxY = screensize.Y - size.Y;
            var y = mousposition.Y + _offset.Y;

            SetPosition(maxX < x ? maxX : x, maxY < y ? maxY : y);
        }
    }
}
