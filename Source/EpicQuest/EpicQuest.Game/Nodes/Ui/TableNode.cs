﻿using System;
using System.Collections.Generic;
using System.Linq;
using EpicQuest.Engine;
using EpicQuest.Engine.SceneTree;
using EpicQuest.Game.Helper;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Game.Nodes.Ui
{
    class TableNode : SceneTreeNode
    {
        private int _padding = 10;

        private readonly List<KeyValuePair<TableRow, List<TextNode>>> _rows = new List<KeyValuePair<TableRow, List<TextNode>>>();

        public void AddRow(TableRow row)
        {
            AddRowToData(row);
            Reorder();
        }

        public void AddRows(IEnumerable<TableRow> rows)
        {
            foreach (var row in rows)
                AddRowToData(row);
            Reorder();
        }

        private void AddRowToData(TableRow row)
        {
            var nodes = new List<TextNode>();

            if (row.IsHeader)
            {
                var node = new TextNode(row.Font ?? GameManager.ContentManager.GetAsset<SpriteFont>(Content.Fonts.DefaultFontHeading), String.Join(" ", row.Data), row.Color ?? Content.Colors.DefaultTextColorHeading);
                nodes.Add(node);
                AddChild(node);
            }
            else
            {
                foreach (var value in row.Data)
                {
                    var node = new TextNode(row.Font ?? GameManager.ContentManager.GetAsset<SpriteFont>(Content.Fonts.DefaultFont), value, row.Color ?? Content.Colors.DefaultTextColor);
                    nodes.Add(node);
                    AddChild(node);
                }
            }


            _rows.Add(new KeyValuePair<TableRow, List<TextNode>>(row, nodes));
        }

        public void RemoveRows()
        {
            foreach (var row in _rows)
                foreach (var node in row.Value)
                    RemoveChild(node);

            _rows.Clear();
            Reorder();
        }

        private void Reorder()
        {
            var spacings = new List<int>{ 0 };
            var posY = 0;

            if (_rows.Any())
            {
                var columnCount = _rows.Max(r => r.Value.Count());

                for (var index = 0; index < columnCount; index++)
                    spacings.Add(spacings.Max()
                                 + _rows
                                    .Where(row => !row.Key.IsHeader)
                                    .Select(row => row.Value)
                                    .Where(c => c.Count > index)
                                    .Select(c => c[index].GetSize().X).Max()
                                 + _padding);

                foreach (var row in _rows)
                {
                    for (var index = 0; index < row.Value.Count; index++)
                        row.Value[index].SetPosition(spacings[index], posY);

                    if(row.Value.Any())
                        posY += row.Value.First().GetSize().Y + _padding;
                }

                spacings.Add(_rows.Any(row => row.Key.IsHeader)
                    ? _rows.Where(row => row.Key.IsHeader).Max(row => row.Value.First().GetSize().X)
                    : 0);
            }

            SetSize(spacings.Max(), posY);
        }

        public class TableRow
        {
            public bool IsHeader { get; set; }
            public Color? Color { get; set; }
            public SpriteFont Font { get; set; }
            public IEnumerable<string> Data { get; set; }
        }
    }
}
