﻿using EpicQuest.Engine.SceneTree;
using EpicQuest.Engine.State;
using EpicQuest.Game.Model;
using Microsoft.Xna.Framework;

namespace EpicQuest.Game.Nodes.OptionNodes
{
    class UnitNode : SceneTreeNode
    {
        private Unit _unit;

        private TextureNode _unitIcon;
        private TextureNode _healthbar;

        public UnitNode(Unit unit, Point position)
        {
            _unit = unit;
            Dimensions = new Rectangle(position, new Point(150, 350));
        }

        protected override void Update(GameState state)
        {
        }
    }
}
