﻿using System;
using EpicQuest.Engine;
using EpicQuest.Engine.ContentLoader;
using EpicQuest.Engine.SceneTree;
using EpicQuest.Game.Helper;
using EpicQuest.Game.Nodes;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Game.Scenes
{
    public class MainMenuScene : BaseScene
    {
        public MainMenuScene(ContentManager contentManager) : base(contentManager)
        {
            contentManager
                .QueueAssetLoad<Texture2D>(Content.MainMenu.Background)
                .QueueAssetLoad<Texture2D>(Content.Defaults.BlankTexture)
                .QueueAssetLoad<SpriteFont>(Content.Fonts.DefaultFontExtraLarge)
                .QueueAssetLoad<SpriteFont>(Content.Fonts.DefaultFontLarge);
        }

        protected override void InitializeSceneTree()
        {
            var background = new TextureNode(GameManager.ContentManager.GetAsset<Texture2D>(Content.MainMenu.Background));
            var title = new TextNode(GameManager.ContentManager.GetAsset<SpriteFont>(Content.Fonts.DefaultFontExtraLarge), Resources.Strings.MainMenu_Title, Content.Colors.ButtonTextColor);
            title.SetPosition(100, 100);

            var buttonStart = new MainMenuButtonNode(Resources.Strings.MainMenu_NewGame);
            buttonStart.SetPosition(150, 250);

            var buttonOptions = new MainMenuButtonNode(Resources.Strings.MainMenu_Options);
            buttonOptions.SetPosition(150, 320);

            var buttonExit = new MainMenuButtonNode(Resources.Strings.MainMenu_Exit);
            buttonExit.SetPosition(150, 390);

            var buttonTest = new MainMenuButtonNode(Resources.Strings.MainMenu_Test);
            buttonTest.SetPosition(150, 980);

            SceneTree.AddChild(background);
            SceneTree.AddChild(title);
            SceneTree.AddChild(buttonStart);
            SceneTree.AddChild(buttonOptions);
            SceneTree.AddChild(buttonExit);
            SceneTree.AddChild(buttonTest);

            buttonExit.OnClick += RequestExitGame;
            buttonOptions.OnClick += SwitchToOptionScene;
            buttonTest.OnClick += SwitchToTestScene;
            buttonStart.OnClick += SwitchToFightScene;
        }

        private void RequestExitGame(object sender, EventArgs args)
        {
            GameManager.GetInstance().RequestExit();
        }

        private void SwitchToTestScene(object sender, EventArgs args)
        {
            GameManager.GetInstance().RequestScene(nameof(TestScene));
        }

        private void SwitchToOptionScene(object sender, EventArgs args)
        {
            GameManager.GetInstance().RequestScene(nameof(OptionsScene));
        }

        private void SwitchToFightScene(object sender, EventArgs args)
        {
            GameManager.GetInstance().RequestScene(nameof(FightScene));
        }
    }
}
