﻿using System;
using System.Collections.Generic;
using System.Linq;
using EpicQuest.Engine;
using EpicQuest.Engine.ContentLoader;
using EpicQuest.Engine.SceneTree;
using EpicQuest.Engine.State;
using EpicQuest.Game.Helper;
using EpicQuest.Game.Nodes;
using EpicQuest.Game.Nodes.OptionNodes;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace EpicQuest.Game.Scenes
{
    public class OptionsScene : BaseScene
    {
        private LeftRightSelectorNode<DisplayMode> _resolutionOptions;
        private LeftRightSelectorNode<bool> _screenmodeOptions;
        private LeftRightSelectorNode<bool> _vsyncOptions;

        private SpriteFont _fontSmall;

        public OptionsScene(ContentManager contentManager) : base(contentManager)
        {
            contentManager
                .QueueAssetLoad<Texture2D>(Content.MainMenu.Background)
                .QueueAssetLoad<Texture2D>(Content.Defaults.BlankTexture)
                .QueueAssetLoad<SpriteFont>(Content.Fonts.DefaultFontExtraLarge)
                .QueueAssetLoad<SpriteFont>(Content.Fonts.DefaultFontLarge);
        }

        protected override void InitializeSceneTree()
        {
            var fontRegular = GameManager.ContentManager.GetAsset<SpriteFont>(Content.Fonts.DefaultFontExtraLarge);
            _fontSmall = GameManager.ContentManager.GetAsset<SpriteFont>(Content.Fonts.DefaultFontLarge);

            var background = new TextureNode(GameManager.ContentManager.GetAsset<Texture2D>(Content.MainMenu.Background));
            var title = new TextNode(fontRegular, Resources.Strings.MainMenu_Title, Content.Colors.ButtonTextColor);
            title.SetPosition(100, 100);

            // Resolution
            var resolutionText = new TextNode(_fontSmall, Resources.Strings.Options_Resolution, Content.Colors.ButtonTextColor);
            resolutionText.SetPosition(200, 250);
            var currentScreenResolution = GameManager.GetInstance().Screen.WindowSize;
            var availableResolutions = GraphicsAdapter.DefaultAdapter.SupportedDisplayModes;
            var resolutionOptions = availableResolutions.Select(r => new KeyValuePair<DisplayMode, string>(r, $"{r.Width} X {r.Height}")).ToArray();
            var currentScreenResolutionOption = availableResolutions.FirstOrDefault(r => r.Width == currentScreenResolution.X && r.Height == currentScreenResolution.Y);
            _resolutionOptions = new LeftRightSelectorNode<DisplayMode>(resolutionOptions, _fontSmall, GameManager.ContentManager.GetAsset<Texture2D>(Content.Defaults.BlankTexture), GameManager.ContentManager.GetAsset<Texture2D>(Content.Defaults.BlankTexture));
            _resolutionOptions.SetPosition(600, 250);
            _resolutionOptions.SetValue(currentScreenResolutionOption);

            // Screenmode
            var screenmodeText = new TextNode(_fontSmall, Resources.Strings.Options_Screenmode, Content.Colors.ButtonTextColor);
            screenmodeText.SetPosition(200, 320);
            var screenoptions = new[]
            {
                new KeyValuePair<bool, string>(true, Resources.Strings.Options_Screenmode_Fullscreen),
                new KeyValuePair<bool, string>(false, Resources.Strings.Options_Screenmode_Windowed)
            };
            _screenmodeOptions = new LeftRightSelectorNode<bool>(screenoptions, _fontSmall, GameManager.ContentManager.GetAsset<Texture2D>(Content.Defaults.BlankTexture), GameManager.ContentManager.GetAsset<Texture2D>(Content.Defaults.BlankTexture));
            _screenmodeOptions.SetPosition(600, 320);
            _screenmodeOptions.SetValue(GameManager.GetInstance().Screen.Fullscreen);

            // VSync
            var vsyncText = new TextNode(_fontSmall, Resources.Strings.Options_VSync, Content.Colors.ButtonTextColor);
            vsyncText.SetPosition(200, 390);
            var vsyncOptions = new[]
            {
                new KeyValuePair<bool, string>(false, Resources.Strings.Options_Disabled),
                new KeyValuePair<bool, string>(true, Resources.Strings.Options_Active)
            };
            _vsyncOptions = new LeftRightSelectorNode<bool>(vsyncOptions, _fontSmall, GameManager.ContentManager.GetAsset<Texture2D>(Content.Defaults.BlankTexture), GameManager.ContentManager.GetAsset<Texture2D>(Content.Defaults.BlankTexture));
            _vsyncOptions.SetPosition(600, 390);
            _vsyncOptions.SetValue(GameManager.GetInstance().Screen.VSync);

            // Save / Cancel
            var buttonSave = new MainMenuButtonNode(Resources.Strings.Options_Save);
            buttonSave.SetPosition(150, 980);

            var buttonCancel = new MainMenuButtonNode(Resources.Strings.Options_Cancel);
            buttonCancel.SetPosition(550, 980);

            SceneTree.AddChild(background);
            SceneTree.AddChild(title);

            SceneTree.AddChild(resolutionText);
            SceneTree.AddChild(_resolutionOptions);

            SceneTree.AddChild(screenmodeText);
            SceneTree.AddChild(_screenmodeOptions);

            SceneTree.AddChild(vsyncText);
            SceneTree.AddChild(_vsyncOptions);

            SceneTree.AddChild(buttonSave);
            SceneTree.AddChild(buttonCancel);

            buttonCancel.OnClick += (sender, args) => GoToMainMenu();
            buttonSave.OnClick += Save;
        }

        protected override void Update(IGameState state)
        {
            if(state.KeyboardState.IsKeyPressed(Keys.Escape))
                GoToMainMenu();
        }

        private void Save(object sender, EventArgs args)
        {
            var settings = new Settings();
            settings.ResolutionWidth = _resolutionOptions.GetCurrentValue().Width;
            settings.ResolutionHeigth = _resolutionOptions.GetCurrentValue().Height;
            settings.Fullscreen = _screenmodeOptions.GetCurrentValue();
            settings.VSYNC = _vsyncOptions.GetCurrentValue();

            FileHandler<Settings>.Save(settings);

            GameManager.GetInstance().Screen.SetResolution(settings.ResolutionWidth.Value, settings.ResolutionHeigth.Value, settings.Fullscreen.Value);
            GameManager.GetInstance().Screen.SetVSync(_vsyncOptions.GetCurrentValue());

            GoToMainMenu();
        }

        private void GoToMainMenu()
        {
            GameManager.GetInstance().RequestScene(nameof(MainMenuScene));
        }
    }
}
