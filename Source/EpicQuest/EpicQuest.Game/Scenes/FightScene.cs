﻿using System.Collections.Generic;
using System.Linq;
using EpicQuest.Engine;
using EpicQuest.Engine.ContentLoader;
using EpicQuest.Engine.SceneTree;
using EpicQuest.Game.Helper;
using EpicQuest.Game.Model;
using EpicQuest.Game.Nodes.Fight;
using Microsoft.Xna.Framework;
using EpicQuest.Engine.EventArgs;
using EpicQuest.Game.Enums;
using EpicQuest.Game.Model.Attacks;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Game.Scenes
{
    public class FightScene : BaseScene
    {
        private UnitGroupNode _playerGroup;
        private UnitGroupNode _enemyGroup;
        private InfoPanelNode _infoPanel;

        private UnitNode _pendingAttackingShip;
        private TurretNode _pendingAttack;

        private List<UnitNode> _availableUnitsThisRound = new List<UnitNode>();

        private FightHoverPanel _hoverPanel;

        private const int FlighInTime = 2000;
        private const int GroupPadding = 50;

        public FightScene(ContentManager contentManager) : base(contentManager)
        {
            contentManager
                .QueueAssetLoad<Texture2D>(Content.Defaults.BlankTexture)
                .QueueAssetLoad<Texture2D>(Content.FightScene.WarriorIcon)
                .QueueAssetLoad<Texture2D>(Content.FightScene.Background)
                .QueueAssetLoad<Texture2D>(Content.FightScene.DefaultTurret)
                .QueueAssetLoad<Texture2D>(Content.FightScene.DefaultBullet)
                .QueueAssetLoad<Texture2D>(Content.FightScene.LaserT1Icon)
                .QueueAssetLoad<Texture2D>(Content.FightScene.LaserT1Bullet)
                .QueueAssetLoad<Texture2D>(Content.FightScene.PlasmaT1Icon)
                .QueueAssetLoad<Texture2D>(Content.FightScene.PlasmaT1Bullet)
                .QueueAssetLoad<SpriteFont>(Content.Fonts.DefaultFontLarge)
                .QueueAssetLoad<SpriteFont>(Content.Fonts.DefaultFont)
                .QueueAssetLoad<SpriteFont>(Content.Fonts.DefaultFontBold)
                .QueueAssetLoad<SoundEffect>(Content.FightScene.LaserT1Sound)
                .QueueAssetLoad<SoundEffect>(Content.FightScene.PlasmaT1Sound)
                .QueueAssetLoad<SoundEffect>(Content.FightScene.DefaultExplosion)
                .QueueAssetLoad<Effect>(Content.Shader.Glow);
        }

        protected override void InitializeSceneTree()
        {
            var screen = GameManager.GetInstance().Screen;

            var background = new TextureNode(GameManager.ContentManager.GetAsset<Texture2D>(Content.FightScene.Background), new Rectangle(0, 0, screen.InternalSize.X, screen.InternalSize.Y), Content.FightScene.BackgroundColor);
            SceneTree.AddChild(background);

            _playerGroup = new UnitGroupNode(Diplomacy.Friend);
            _playerGroup.AddUnit(new Point(0, 0), new Warrior("Saber"));
            _playerGroup.AddUnit(new Point(1, 1), new Warrior("Onyx"));
            _playerGroup.AddUnit(new Point(0, 2), new Warrior("Scythe"));
            _playerGroup.SetPosition(-_playerGroup.GetSize().X, GroupPadding);
            _playerGroup.SetPosition(GroupPadding, GroupPadding, FlighInTime, StartRound);
            SceneTree.AddChild(_playerGroup);

            _enemyGroup = new UnitGroupNode(Diplomacy.Enemy);
            _enemyGroup.AddUnit(new Point(0, 0), new Warrior("DF-659"));
            _enemyGroup.AddUnit(new Point(1, 1), new Warrior("HT-047"));
            _enemyGroup.AddUnit(new Point(0, 2), new Warrior("XF-009"));
            _enemyGroup.SetPosition(screen.InternalSize.X, GroupPadding);
            _enemyGroup.SetPosition(screen.InternalSize.X - (GroupPadding + _enemyGroup.GetSize().X), GroupPadding, FlighInTime);
            SceneTree.AddChild(_enemyGroup);

            _infoPanel = new InfoPanelNode(GameManager.ContentManager.GetAsset<Texture2D>(Content.Defaults.BlankTexture), GameManager.ContentManager.GetAsset<SpriteFont>(Content.Fonts.DefaultFontLarge), GameManager.ContentManager.GetAsset<SpriteFont>(Content.Fonts.DefaultFont));
            _infoPanel.OnAttackSelected += StartAttack;
            _infoPanel.OnAttackHoverStart += AttackHoverStart;
            _infoPanel.OnAttackHoverEnd += AttackHoverEnd;
            SceneTree.AddChild(_infoPanel);

            _hoverPanel = new FightHoverPanel();
            SceneTree.AddChild(_hoverPanel);
        }

        private void StartRound()
        {
            DisplayUnitHoverPanel(true);

            _playerGroup.UnitNodeSelected += PlayerUnitSelected;
            _enemyGroup.UnitNodeSelected += EnemyUnitSelected;

            _availableUnitsThisRound = _playerGroup.GetLivingUnits().ToList();
            SetUnitGroupSelectable(_availableUnitsThisRound, true);
        }

        private void PlayerUnitSelected(object sender, PayloadEventArgs<UnitNode> payloadEventArgs)
        {
            SetUnitGroupSelected(_playerGroup.GetUnits(), false);

            _infoPanel.SetAttackingUnit(payloadEventArgs.Payload.Unit);
            _pendingAttackingShip = payloadEventArgs.Payload;

            payloadEventArgs.Payload.Selected = true;
        }

        private void StartAttack(object sender, PayloadEventArgs<TurretNode> e)
        {
            SetUnitGroupSelectable(_playerGroup.GetUnits(), false);
            SetUnitGroupSelectable(_enemyGroup.GetLivingUnits(), true);

            _pendingAttack = e.Payload;

            DisplayUnitHoverPanel(false);
            DisplayAttackUnitHoverPanel(true);
        }

        private void EnemyUnitSelected(object o, PayloadEventArgs<UnitNode> payloadEventArgs)
        {
            SetUnitGroupSelectable(_enemyGroup.GetLivingUnits(), false);
            _pendingAttack.AttackShip(payloadEventArgs.Payload, () => FinishEnemyUnitSelected(payloadEventArgs));
        }

        private void FinishEnemyUnitSelected(PayloadEventArgs<UnitNode> payloadEventArgs)
        {
            _availableUnitsThisRound.Remove(_pendingAttackingShip);
            SetUnitGroupSelectable(_availableUnitsThisRound, true);
            SetUnitGroupSelected(_availableUnitsThisRound, false);

            DisplayAttackUnitHoverPanel(false);
            DisplayUnitHoverPanel(true);

            AttackUnitHoverEnd(this, null);

            _pendingAttackingShip = null;
            _pendingAttack = null;
            _infoPanel.SetAttackingUnit(null);

            if(!_availableUnitsThisRound.Any())
                StartEnemyAttack();
        }

        private void StartEnemyAttack()
        {
            var enemies = _enemyGroup.GetLivingUnits().ToList();
            foreach (var enemy in enemies)
                enemy.Selectable = true;

            PerformEnemyAttack();
        }

        private void PerformEnemyAttack()
        {
            var enemy = _enemyGroup.GetLivingUnits().FirstOrDefault(u => u.Selectable);
            var victim = _playerGroup.GetLivingUnits().OrderBy(u => Randomizer.Random(100)).FirstOrDefault();

            if (enemy != null && victim != null)
            {
                var turret = enemy.Unit.Turrets.OrderBy(u => Randomizer.Random(100)).First();
                turret.AttackShip(victim, () =>
                {
                    enemy.Selectable = false;
                    PerformEnemyAttack();
                });
            }
            else
            {
                FinishEnemyAttack();
            }
        }

        private void FinishEnemyAttack()
        {
            _availableUnitsThisRound = _playerGroup.GetLivingUnits().ToList();
            SetUnitGroupSelectable(_availableUnitsThisRound, true);
        }

        private void DisplayUnitHoverPanel(bool display)
        {
            if (display)
            {
                DisplayUnitHoverPanel(false);
                _playerGroup.UnitNodeHoverStart += UnitHoverStart;
                _playerGroup.UnitNodeHoverEnd += UnitHoverEnd;

                _enemyGroup.UnitNodeHoverStart += UnitHoverStart;
                _enemyGroup.UnitNodeHoverEnd += UnitHoverEnd;
            }
            else
            {
                _playerGroup.UnitNodeHoverStart -= UnitHoverStart;
                _playerGroup.UnitNodeHoverEnd -= UnitHoverEnd;

                _enemyGroup.UnitNodeHoverStart -= UnitHoverStart;
                _enemyGroup.UnitNodeHoverEnd -= UnitHoverEnd;
            }
        }

        private void DisplayAttackUnitHoverPanel(bool display)
        {
            if (display)
            {
                DisplayAttackUnitHoverPanel(false);
                _enemyGroup.UnitNodeHoverStart += AttackUnitHoverStart;
                _enemyGroup.UnitNodeHoverEnd += AttackUnitHoverEnd;
            }
            else
            {
                _enemyGroup.UnitNodeHoverStart -= AttackUnitHoverStart;
                _enemyGroup.UnitNodeHoverEnd -= AttackUnitHoverEnd;
            }
        }

        private void UnitHoverStart(object sender, PayloadEventArgs<UnitNode> payloadEventArgs)
        {
            _hoverPanel.DisplayUnitData(payloadEventArgs.Payload.Unit);
            _hoverPanel.SetVisibility(true);
        }

        private void UnitHoverEnd(object sender, PayloadEventArgs<UnitNode> payloadEventArgs)
        {
            _hoverPanel.SetVisibility(false);
        }

        private void AttackHoverStart(object sender, PayloadEventArgs<TurretNode> payloadEventArgs)
        {
            _hoverPanel.DisplayAttackData(payloadEventArgs.Payload.Attack);
            _hoverPanel.SetVisibility(true);
        }

        private void AttackHoverEnd(object sender, PayloadEventArgs<TurretNode> payloadEventArgs)
        {
            _hoverPanel.SetVisibility(false);
        }

        private void AttackUnitHoverStart(object sender, PayloadEventArgs<UnitNode> payloadEventArgs)
        {
            _hoverPanel.DisplayAttackResultData(_pendingAttack.Attack, payloadEventArgs.Payload.Unit);
            _hoverPanel.SetVisibility(true);
        }

        private void AttackUnitHoverEnd(object sender, PayloadEventArgs<UnitNode> payloadEventArgs)
        {
            _hoverPanel.SetVisibility(false);
        }

        private void SetUnitGroupSelectable(IEnumerable<UnitNode> units, bool selectable)
        {
            foreach (var node in units)
                node.Selectable = selectable;
        }

        private void SetUnitGroupSelected(IEnumerable<UnitNode> units, bool selected)
        {
            foreach (var node in units)
                node.Selected = selected;
        }
    }
}
