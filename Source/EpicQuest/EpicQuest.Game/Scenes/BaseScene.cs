﻿using EpicQuest.Engine;
using EpicQuest.Engine.ContentLoader;
using EpicQuest.Game.Helper;

namespace EpicQuest.Game.Scenes
{
    public abstract class BaseScene : Scene
    {
        protected BaseScene(ContentManager contentManager) : base(contentManager)
        {
            CursorTexture = Content.Defaults.Cursor;

            var settings = FileHandler<Settings>.Load();

            GameManager.GetInstance().Screen
                .SetResolution(settings?.ResolutionWidth ?? 1920, settings?.ResolutionHeigth ?? 1080, settings?.Fullscreen ?? true)
                .SetVSync(settings?.VSYNC ?? false);
        }
    }
}
