﻿using System;
using EpicQuest.Engine;
using EpicQuest.Engine.ContentLoader;
using EpicQuest.Engine.SceneTree;
using EpicQuest.Engine.State;
using EpicQuest.Game.Helper;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Game.Scenes
{
    public class TestScene : BaseScene
    {
        private int _direction = 1;
        private TextNode _text;

        private TextureNode _shakingSquare;

        private TextureNode _moveTestHorizontal;
        private TextureNode _moveTestVertical;

        private TextureNode _moveTestDiagonal;

        private TextureNode _clockSeconds;
        private TextureNode _clockMilliseconds;

        private TextButtonNode _testButton;
        private TextButtonNode _anyway;

        private bool _moveBlueRight = true;
        private SpriteFont fontSmall;

        private float _timer;

        public TestScene(ContentManager contentManager) : base(contentManager)
        {
            contentManager
                .QueueAssetLoad<Texture2D>(Content.TestScene.Background)
                .QueueAssetLoad<Texture2D>(Content.Defaults.BlankTexture)
                .QueueAssetLoad<Texture2D>(Content.FightScene.WarriorIcon)
                .QueueAssetLoad<SpriteFont>(Content.Fonts.DefaultFontExtraLarge)
                .QueueAssetLoad<SpriteFont>(Content.Fonts.DefaultFontLarge)
                .QueueAssetLoad<SoundEffect>(Content.TestScene.Test_Sound)
                .QueueAssetLoad<Effect>(Content.Shader.Glow);
        }

        protected override void InitializeSceneTree()
        {
            var background = GameManager.ContentManager.GetAsset<Texture2D>(Content.TestScene.Background);
            var backgroundNode = new TextureNode(background);
            backgroundNode.Color = Color.Gray;
            backgroundNode.RotateTo(5);
            backgroundNode.RotateBy(5);

            SceneTree.AddChild(backgroundNode);

            var fontLarge = GameManager.ContentManager.GetAsset<SpriteFont>(Content.Fonts.DefaultFontExtraLarge);
                fontSmall = GameManager.ContentManager.GetAsset<SpriteFont>(Content.Fonts.DefaultFontLarge);
            _text = new TextNode(fontLarge, "SampleText", Color.DarkRed);
            _text.SetPosition(20, 10);
            SceneTree.AddChild(_text);

            var square = GameManager.ContentManager.GetAsset<Texture2D>(Content.Defaults.BlankTexture);

            _testButton = new TextButtonNode("Random Color", fontSmall, Color.Red);
            _testButton.SetPosition(50, 120);
            _testButton.SetSize(350, 50);
            _testButton.RotateTo(5);
            _testButton.SetActiveText("> Random Color <", fontSmall, Color.CornflowerBlue);
            _testButton.SetBackground(square, Color.CornflowerBlue);
            _testButton.SetActiveBackground(square, Color.Red);

            var mainmenuButton = new TextButtonNode("MainMenu", fontSmall, Color.Black);
            mainmenuButton.SetPosition(50, 200);
            mainmenuButton.SetSize(350, 50);
            mainmenuButton.SetBackground(square, Color.CornflowerBlue);
            mainmenuButton.SetActiveBackground(square, Color.Red);

            var noHoverButton = new TextButtonNode("No Hover Effect :(", fontSmall, Color.Black);
            noHoverButton.SetPosition(50, 250);
            noHoverButton.SetSize(350, 50);
            noHoverButton.SetBackground(square, Color.CornflowerBlue);

            var onlyHoverButton = new TextButtonNode("Only Hover :D", fontSmall, Color.Black);
            onlyHoverButton.SetPosition(50, 350);
            onlyHoverButton.SetSize(350, 50);
            onlyHoverButton.SetActiveBackground(square, Color.CornflowerBlue);

            var otherway = new TextButtonNode("other way around", fontSmall, Color.Black);
            otherway.SetPosition(450, 200);
            otherway.SetSize(350, 50);
            otherway.SetBackground(square, Color.CornflowerBlue);
            otherway.SetActiveBackground(square, Color.Green);

            _anyway = new TextButtonNode(Resources.Strings.Test_CrazyButton, fontSmall, Color.Black);
            _anyway.SetPosition(50, 300);
            _anyway.SetSize(350, 50);
            _anyway.SetBackground(square, Color.CornflowerBlue);

            mainmenuButton.OnClick += (sender, args) => GameManager.GetInstance().RequestScene(nameof(MainMenuScene));

            var testSoundButton = new TextButtonNode("Testsound", fontSmall, Color.Black);
            testSoundButton.SetPosition(50, 400);
            testSoundButton.SetSize(350, 50);
            testSoundButton.SetBackground(square, Color.CornflowerBlue);
            testSoundButton.OnClick += (senter, args) => GameManager.ContentManager.GetAsset<SoundEffect>(Content.TestScene.Test_Sound).Play();

            SceneTree.AddChild(_testButton);
            SceneTree.AddChild(mainmenuButton);
            SceneTree.AddChild(noHoverButton);
            SceneTree.AddChild(onlyHoverButton);
            SceneTree.AddChild(otherway);
            SceneTree.AddChild(_anyway);
            SceneTree.AddChild(testSoundButton);

            _testButton.OnClick += OnTestButtonClick;
            otherway.OnClick += OnOtherwayButtonClick;
            var input = new InputNode(new Rectangle(450, 150, 350, 50), square, fontSmall, Color.Black);
            input.SetPosition(450, 150);
            SceneTree.AddChild(input);

            _shakingSquare = new TextureNode(square, Color.HotPink);
            _shakingSquare.SetPosition(500, 500);
            SceneTree.AddChild(_shakingSquare);
            MoveShakingSquare();

            _moveTestHorizontal = new TextureNode(square, Color.HotPink);
            _moveTestHorizontal.SetPosition(500, 1000);
            _moveTestHorizontal.RotateTo(-45);

            _moveTestVertical = new TextureNode(square, Color.HotPink);

            SceneTree.AddChild(_moveTestHorizontal);
            _moveTestHorizontal.AddChild(_moveTestVertical);

            MoveTestRight();
            MoveTestUp();

            _moveTestDiagonal = new TextureNode(square, Color.HotPink);
            _moveTestDiagonal.SetPosition(0, 1060);

            SceneTree.AddChild(_moveTestDiagonal);
            MoveTestDiagonalRight();

            _clockSeconds = new TextureNode(square, Color.Goldenrod);
            _clockSeconds.SetPosition(900, 400);
            _clockSeconds.SetSize(6, 80);
            _clockSeconds.SetOrigin(3, 3);
            SceneTree.AddChild(_clockSeconds);

            _clockMilliseconds = new TextureNode(square, Color.Aqua);
            _clockMilliseconds.SetSize(5, 40);
            _clockMilliseconds.SetPosition(0, 77);
            _clockMilliseconds.SetOrigin(3, 6);
            _clockSeconds.AddChild(_clockMilliseconds);
        }

        protected override void Update(IGameState state)
        {
            if (_timer > 1000)
            {
                Random rng = new Random();
                _anyway.SetText(Resources.Strings.Test_CrazyButton, fontSmall, new Color(rng.Next(255), rng.Next(255), rng.Next(255), 255));
                _timer = 0;
            }
            else
            {
                _timer += state.GameTime.ElapsedGameTime.Milliseconds;
            }

            var rotationSeconds = 180 + DateTime.Now.Second * 6;
            _clockSeconds.RotateTo(rotationSeconds);
            _clockMilliseconds.RotateTo(DateTime.Now.Millisecond * 360f / 1000f - rotationSeconds + 180);
        }

        private void OnTestButtonClick(object sender, EventArgs e)
        {
            _text.SetColor(new Color(Randomizer.Random(255), Randomizer.Random(255), Randomizer.Random(255), 255));
        }
        private void OnOtherwayButtonClick(object send, EventArgs e)
        {
            _direction = _direction * -1;
        }

        private void MoveShakingSquare()
        {
            var origin = new Point(500, 500);
            var offset = new Point(Randomizer.Random(20) - 10, Randomizer.Random(20) - 10);
            var t = origin + offset;
            _shakingSquare.SetPosition(t.X, t.Y, 500, MoveShakingSquare);
        }

        private void MoveTestRight()
        {
            _moveTestHorizontal.Move(200, 0, 2000, MoveTestLeft);
        }

        private void MoveTestLeft()
        {
            _moveTestHorizontal.Move(-200, 0, 2000, MoveTestRight);
        }

        private void MoveTestUp()
        {
            _moveTestVertical.Move(0, -200, 1000, MoveTestDown);
        }

        private void MoveTestDown()
        {
            _moveTestVertical.Move(0, 200, 1000, MoveTestUp);
        }

        private void MoveTestDiagonalRight()
        {
            _moveTestDiagonal.SetPosition(1900, 0, 2000, MoveTestDiagonalLeft);
        }

        private void MoveTestDiagonalLeft()
        {
            _moveTestDiagonal.SetPosition(0, 1060, 2000, MoveTestDiagonalRight);
        }
    }
}
