﻿namespace EpicQuest.Game
{
    public class Settings
    {
        public int? ResolutionWidth { get; set; }
        public int? ResolutionHeigth { get; set; }
        public bool? Fullscreen { get; set; }
        public bool? VSYNC { get; set; }
    }
}
