﻿using System;
using System.Linq;
using EpicQuest.Engine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Game.Shader
{
    class GlowShader : Engine.Shader
    {
        public Color? Color { get; set; }

        private const int MaxKernelStepCount = 15;
        private const float Amplifier = 2;

        private readonly Effect _effect;
        private readonly Texture2D _texture;
        private readonly int _kernelCount;
        private Vector2[] _kernelStepsHorizontal;
        private Vector2[] _kernelStepsVertical;
        private float[] _kernelWeigths;

        private readonly RenderTarget2D _blurVerticalTarget;
        private readonly RenderTarget2D _blurHorizontalTarget;

        public GlowShader(Effect effect, Texture2D texture, int stepCount = 7, float sigma = 10f)
        {
            _effect = effect;
            _kernelCount = stepCount * 2 + 1;
            _texture = texture;

            if (_kernelCount > MaxKernelStepCount)
                _kernelCount = MaxKernelStepCount;

            CalculateGuassianKernelWeights(sigma);
            CalculateKernelSteps(texture);

            _blurVerticalTarget = new RenderTarget2D(GameManager.GetInstance().Screen.GraphicsDevice, texture.Width, texture.Height);
            _blurHorizontalTarget = new RenderTarget2D(GameManager.GetInstance().Screen.GraphicsDevice, texture.Width, texture.Height);
        }

        double Guassian(int x, double sigma)
        {
            var exponent = -(Math.Pow(x, 2) / Math.Pow(sigma, 2));
            var factor = 1 / (Math.Sqrt(Math.PI * sigma));

            return factor * Math.Exp(exponent);
        }

        void CalculateGuassianKernelWeights(double sigma)
        {
            var offset = (MaxKernelStepCount - _kernelCount) / 2;

            _kernelWeigths = new float[MaxKernelStepCount];
            for (int i = 0; i < _kernelCount; ++i)
                _kernelWeigths[i + offset] = (float)Guassian(i - _kernelCount / 2, sigma);

            var sum = _kernelWeigths.Sum();

            for (int i = 0; i < _kernelCount; ++i)
                _kernelWeigths[i] = _kernelWeigths[i] / sum;
        }

        void CalculateKernelSteps(Texture2D texture)
        {
            _kernelStepsHorizontal = new Vector2[MaxKernelStepCount];
            _kernelStepsVertical = new Vector2[MaxKernelStepCount];

            var kernelStepSizeHorizontal = 1f / texture.Width;
            var kernelStepSizeVertical = 1f / texture.Height;

            for (int i = 0; i < MaxKernelStepCount; i++)
            {
                _kernelStepsHorizontal[i].X = (i - MaxKernelStepCount / 2) * kernelStepSizeHorizontal;
                _kernelStepsVertical[i].Y = (i - MaxKernelStepCount / 2) * kernelStepSizeVertical;
            }
        }

        public override void PreDraw(SpriteBatch batch)
        {
            _effect.Parameters["USE_CUSTOM_COLOR"].SetValue(Color.HasValue);
            if (Color.HasValue)
                _effect.Parameters["CUSTOM_COLOR"].SetValue(Color.Value.ToVector4());

            _effect.Parameters["KERNEL_WEIGHT"].SetValue(_kernelWeigths);
            _effect.Parameters["AMPLIFIER"].SetValue(Amplifier);

            _effect.Parameters["KERNEL_STEPS"].SetValue(_kernelStepsHorizontal);
            ApplyPass(batch, "Blur", _texture, _blurHorizontalTarget);

            _effect.Parameters["KERNEL_STEPS"].SetValue(_kernelStepsVertical);
            ApplyPass(batch, "Blur", _blurHorizontalTarget, _blurVerticalTarget);
        }

        private void ApplyPass(SpriteBatch batch, string pass, Texture2D texture, RenderTarget2D target)
        {
            var screen = GameManager.GetInstance().Screen;

            screen.GraphicsDevice.SetRenderTarget(target);
            screen.GraphicsDevice.Clear(Microsoft.Xna.Framework.Color.Transparent);
            screen.InitiateDraw(batch, null, false);

            _effect.CurrentTechnique.Passes[pass].Apply();

            batch.Draw(texture, Vector2.Zero, Microsoft.Xna.Framework.Color.White);

            screen.EndDraw(batch);
            screen.GraphicsDevice.SetRenderTarget(null);
        }

        public override Texture2D GetPreDrawShaderResult()
        {
            return _blurVerticalTarget;
        }
    }
}
