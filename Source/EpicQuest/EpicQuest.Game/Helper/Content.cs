﻿using EpicQuest.Engine;
using Microsoft.Xna.Framework;

namespace EpicQuest.Game.Helper
{
    class Content : DefaultContent
    {
        public static class Fonts
        {
            public const string DefaultFontExtraLarge = "Fonts/DefaultFont60pt";
            public const string DefaultFontLarge = "Fonts/DefaultFont20pt";
            public const string DefaultFont = "Fonts/DefaultFont15pt";
            public const string DefaultFontBold = "Fonts/DefaultFont15ptBold";

            public const string DefaultFontHeading = DefaultFontBold;
        }

        public static class Colors
        {
            public static Color DefaultTextColorHeading = Color.CornflowerBlue;
            public static Color DefaultTextColor = Color.White;

            public static Color ButtonTextColor = DefaultTextColorHeading;
            public static Color ButtonColor = Color.White * 0.4f;
            public static Color ButtonHoverColor = Color.Black * 0.4f;
        }

        public static class MainMenu
        {
            public const string Background = "MainMenu/background";
        }

        public static class TestScene
        {
            public const string Background = "Test/fullhd_test";
            public const string Test_Sound = "Test/test_Sound";
        }

        public static class FightScene
        {
            public static Color BackgroundColor = Color.DarkGray;
            public static Color HealthbarForeground = Color.Red;
            public static Color HealthbarBackground = Color.Black;
            public static Color ShieldbarForeground = Color.Blue;
            public static Color ShieldbarBackground = Color.Black;


            public static Color AttackFailed = Color.LimeGreen;
            public static Color AttackSuccessfull = Color.Orange;
            public static Color AttackCritical = Color.Red;

            public static Color UnitFriend = Color.CornflowerBlue;
            public static Color SelectableUnitFriend = Color.AliceBlue;
            public static Color SelectedUnitFriend = Color.DodgerBlue;
            public static Color SelectableUnitFriendHovered = UnitFriend;

            public static Color UnitEnemy = Color.Red;
            public static Color SelectableUnitEnemy = Color.DarkRed;
            public static Color SelectedUnitEnemy = Color.DarkRed;
            public static Color SelectableUnitEnemyHovered = UnitEnemy;

            public static Color DeadUnit = new Color(40, 40, 40);

            public static Color HoverPanelBackgroundColor = Color.Black * .9f;

            public static Color BorderColor = Color.CornflowerBlue;

            public const string WarriorIcon = "Units/warrior";

            public const string Background = "FightScene/Background/space";

            public const string DefaultTurret = "Attacks/DefaultTurret";
            public const string DefaultBullet = "Attacks/DefaultBullet";
            public const string DefaultExplosion = "Attacks/DefaultExplosionSound";

            public const string LaserT1Icon = "Attacks/LaserT1Icon";
            public const string PlasmaT1Icon = "Attacks/PlasmaT1Icon"; 

            public const string LaserT1Bullet = "Attacks/LaserT1Bullet";
            public const string PlasmaT1Bullet = "Attacks/PlasmaT1Bullet";

            public const string LaserT1Sound = "Attacks/LaserT1Sound";
            public const string PlasmaT1Sound = "Attacks/PlasmaT1Sound";

        }

        public static class Shader
        {
            public const string Glow = "Shader/Glow";
        }
    }
}
