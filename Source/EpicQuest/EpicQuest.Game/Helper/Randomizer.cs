﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpicQuest.Game.Helper
{
    class Randomizer
    {
        private static Random _rng;

        private static Random GetRng()
        {
            return _rng ?? (_rng = new Random());
        }

        public static int Random(int max)
        {
            return GetRng().Next(max);
        }

        public static int Random(int min, int max)
        {
            return GetRng().Next(min, max);
        }

        public static bool Successfull(float chance)
        {
            return GetRng().Next(100 * 100) <= chance * 100;
        }

        public static bool Failed(float chance)
        {
            return !Successfull(chance);
        }
    }
}
