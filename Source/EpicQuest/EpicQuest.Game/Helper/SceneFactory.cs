﻿using EpicQuest.Engine;
using EpicQuest.Engine.ContentLoader;
using EpicQuest.Game.Scenes;

namespace EpicQuest.Game.Helper
{
    public class SceneFactory : ISceneFactory
    {
        public Scene GetScene(string scenetype, ContentManager contentManager)
        {
            switch (scenetype)
            {
                case nameof(TestScene):
                    return new TestScene(contentManager);
                case nameof(OptionsScene):
                    return new OptionsScene(contentManager);
                case nameof(FightScene):
                    return new FightScene(contentManager);
                case nameof(MainMenuScene):
                default:
                    return new MainMenuScene(contentManager);
            }
        }
    }
}
