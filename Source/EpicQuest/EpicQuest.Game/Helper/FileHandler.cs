﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace EpicQuest.Game
{
    class FileHandler<T>
    {
        public static T Load()
        {
            return Load(GetFilenameFromType());
        }

        public static T Load(string filename)
        {
            var result = default(T);

            if (File.Exists(filename))
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(filename);
                string xmlString = xmlDocument.OuterXml;

                using (StringReader read = new StringReader(xmlString))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(T));
                    using (XmlReader reader = new XmlTextReader(read))
                    {
                        result = (T)serializer.Deserialize(reader);
                        reader.Close();
                    }
                    read.Close();
                }
            }

            return result;
        }

        public static bool CanLoad()
        {
            return CanLoad(GetFilenameFromType());
        }

        public static bool CanLoad(string filename)
        {
            return File.Exists(filename);
        }

        private static string GetFilenameFromType()
        {
            return typeof(T).Name + ".xml";
        }

        public static void Save(T toBeSaved)
        {
            Save(GetFilenameFromType(), toBeSaved);
        }

        public static void Save(string filename, T toBeSaved)
        {
            var xmlDocument = new XmlDocument();
            var serializer = new XmlSerializer(typeof(T));

            using (MemoryStream stream = new MemoryStream())
            {
                serializer.Serialize(stream, toBeSaved);
                stream.Position = 0;
                xmlDocument.Load(stream);
                xmlDocument.Save(filename);
                stream.Close();
            }
        }
    }
}
