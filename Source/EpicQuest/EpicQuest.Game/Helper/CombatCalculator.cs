﻿using System;
using EpicQuest.Game.Model;
using EpicQuest.Game.Model.Attacks;

namespace EpicQuest.Game.Helper
{
    public class CombatCalculator
    {
        public static AttackResultDo CalculateAttackResult(Attack attack, Unit defender)
        {
            var result = new AttackResultDo();

            if (Randomizer.Successfull(defender.EvasionRate))
                result.Evaded = true;
            else if (Randomizer.Failed(attack.HitChance))
                result.Missed = true;
            else
                result.Damage = Randomizer.Random(attack.MinDamage, attack.MaxDamage);

            if (result.Damage > 0 && Randomizer.Successfull(attack.CritChance))
            {
                result.Critical = true;
                result.Damage *= (int)Math.Floor(attack.CritModifier);
            }

            return result;
        }

        public static AttackStatisticsDo CalculateAttackStatistics(Attack attack, Unit defender)
        {
            return new AttackStatisticsDo
            {
                HitChance = attack.HitChance * (100 - defender.EvasionRate) / 100
            };
        }
    }
}
