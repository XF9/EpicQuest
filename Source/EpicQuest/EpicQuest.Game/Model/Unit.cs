﻿using System.Collections.Generic;
using System.Data.SqlTypes;
using EpicQuest.Game.Enums;
using EpicQuest.Game.Helper;
using EpicQuest.Game.Model.Attacks;

namespace EpicQuest.Game.Model
{
    public abstract class Unit
    {
        public abstract string GetUnitImage();

        public string Name { get; protected set; }

        public int Hitpoints { get; protected set; }
        public int MaximumHitpoints { get; protected set; }
        public int Shield { get; protected set; }
        public int MaximumShield { get; protected set; }

        public List<TurretNode> Turrets { get; protected set; }

        public float EvasionRate { get; protected set; }

        public Diplomacy Diplomacy { get; set; }

        public void TakeDamage(int damage)
        {
            if(Shield > 0)
            {
                Shield -= damage;
                if(Shield < 0)
                {
                    Hitpoints += Shield;
                    Shield = 0;
                }
            }else
            {
                Hitpoints -= damage;
            }


            if (Hitpoints < 0)
                Hitpoints = 0;
        }

        public bool IsDead()
        {
            return Hitpoints <= 0;
        }

        public Unit()
        {
            Turrets = new List<TurretNode>();
        }
    }
}
