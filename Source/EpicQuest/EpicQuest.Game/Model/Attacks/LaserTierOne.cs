﻿using EpicQuest.Game.Helper;
using Microsoft.Xna.Framework;

namespace EpicQuest.Game.Model.Attacks
{
    public class LaserTierOne : Attack
    {
        public LaserTierOne()
        {
            Name = "Laser T1";
            MinDamage = 15;
            MaxDamage = 20;
            HitChance = 90.5f;
            CritChance = 10.8f;
            CritModifier = 2.5f;

            Color = Color.LimeGreen;
            Icon = Content.FightScene.LaserT1Icon;
            Bullet = Content.FightScene.LaserT1Bullet;
            Sound = Content.FightScene.LaserT1Sound;
            Velocity = 1.5f;
        }
    }
}
