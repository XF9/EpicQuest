﻿using EpicQuest.Game.Helper;
using Microsoft.Xna.Framework;

namespace EpicQuest.Game.Model.Attacks
{
    public class PlasmaTierOne : Attack
    {
        public PlasmaTierOne()
        {
            Name = "Plasma T1";
            MinDamage = 30;
            MaxDamage = 40;
            HitChance = 40.5f;
            CritChance = 5.4f;
            CritModifier = 2.5f;

            Color = Color.Aqua;
            Icon = Content.FightScene.PlasmaT1Icon;
            Bullet = Content.FightScene.PlasmaT1Bullet;
            Sound = Content.FightScene.PlasmaT1Sound;
            Velocity = .5f;
        }
    }
}
