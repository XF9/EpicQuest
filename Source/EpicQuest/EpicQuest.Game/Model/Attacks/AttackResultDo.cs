﻿namespace EpicQuest.Game.Model.Attacks
{
    public class AttackResultDo
    {
        public bool Missed { get; set; }
        public bool Evaded { get; set; }
        public bool Critical { get; set; }
        public int Damage { get; set; }
    }
}
