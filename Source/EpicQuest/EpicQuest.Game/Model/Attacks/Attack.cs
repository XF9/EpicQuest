﻿using EpicQuest.Engine;
using EpicQuest.Game.Helper;
using Microsoft.Xna.Framework;

namespace EpicQuest.Game.Model.Attacks
{
    public abstract class Attack
    {
        public string Name { get; set; }
        public int MinDamage { get; protected set; }
        public int MaxDamage { get; protected set; }
        public float HitChance { get; protected set; }
        public float CritChance { get; protected set; }
        public float CritModifier { get; protected set; } = 2;

        public string Icon { get; protected set; } = DefaultContent.Defaults.BlankTexture;
        public string Sound { get; protected set; }
        public string ExplosionSound { get; protected set; } = Content.FightScene.DefaultExplosion;
        public string Turret { get; protected set; } = Content.FightScene.DefaultTurret;
        public string Bullet { get; protected set; } = Content.FightScene.DefaultBullet;
        public Color Color { get; protected set; } = Color.White;
        public float Velocity { get; set; } = 1.5f;
    }
}
