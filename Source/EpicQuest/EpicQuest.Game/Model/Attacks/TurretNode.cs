﻿using System;
using EpicQuest.Engine;
using EpicQuest.Engine.SceneTree;
using EpicQuest.Game.Helper;
using EpicQuest.Game.Nodes.Fight;
using EpicQuest.Game.Shader;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Game.Model.Attacks
{
    public class TurretNode : SceneTreeNode
    {
        private Point _position;
        public Point Position
        {
            get => _position;
            set
            {
                _position = value;
                SetPosition(Position.X, Position.Y);
            }
        }

        private Point _origin;
        public Point Origin
        {
            get => _origin;
            set
            {
                _origin = value;
                SetOrigin(Origin.X, Origin.Y);
            }
        }

        private SceneTreeNode _turretNode;
        private Attack _attack;
        public Attack Attack
        {
            get => _attack;
            set
            {
                _attack = value;
                _turretNode = new TextureNode(GameManager.ContentManager.GetAsset<Texture2D>(Attack.Turret), _attack.Color);
                AddChild(_turretNode);
            }
        }

        public void AttackShip(UnitNode enemy, Action animationDone = null)
        {
            if (_turretNode == null)
                animationDone?.Invoke();

            var enemyLocation = enemy.GlobalTransformation.Translation + enemy.UnitCenter;
            var turretLocation = GlobalTransformation.Translation;
            var turretRotation = GlobalTransformation.Rotation;

            var distance = Math.Sqrt(Math.Pow(enemyLocation.X - turretLocation.X, 2) +
                                     Math.Pow(enemyLocation.Y - turretLocation.Y, 2));

            bool targetIsLeft = turretLocation.X > enemyLocation.X;

            var angle = MathHelper.ToDegrees((float) Math.Asin((enemyLocation.Y - turretLocation.Y) / distance));
            if (targetIsLeft)
                angle = 180 - angle;

            angle -= turretRotation;

            RotateTo(angle, 500, () =>
            {
                var bulletTexture = GameManager.ContentManager.GetAsset<Texture2D>(Attack.Bullet);
                var bulletSound = GameManager.ContentManager.GetAsset<SoundEffect>(Attack.Sound);
                var bulletExplosion = GameManager.ContentManager.GetAsset<SoundEffect>(Attack.ExplosionSound);
                var bullet = new TextureNode(bulletTexture, Attack.Color);
                var shader = new GlowShader(GameManager.ContentManager.GetAsset<Effect>(Content.Shader.Glow), bulletTexture, 2)
                {
                    Color = Attack.Color
                };
                bullet.Shader = shader;
                AddChild(bullet);
                bulletSound.Play();
                bullet.Move((float) distance, 0, (int)(distance / Attack.Velocity), () =>
                {
                    RemoveChild(bullet);
                    var combatResult = CombatCalculator.CalculateAttackResult(Attack, enemy.Unit);
                    if(!combatResult.Missed && !combatResult.Evaded)
                        bulletExplosion.Play();
                    enemy.ApplyCombatResult(combatResult);

                    RotateTo(0, 500, () =>
                    {
                        animationDone?.Invoke();
                    });
                });
            });
        }
    }
}