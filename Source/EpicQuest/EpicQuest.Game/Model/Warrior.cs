﻿using EpicQuest.Game.Helper;
using EpicQuest.Game.Model.Attacks;
using Microsoft.Xna.Framework;

namespace EpicQuest.Game.Model
{
    class Warrior : Unit
    {
        public Warrior(string name = "Warrior")
        {
            Name = name;
            MaximumHitpoints = 120;
            Hitpoints = 120;
            Shield = 100;
            MaximumShield = 100;
            EvasionRate = 10.5f;

            Turrets.Add(new TurretNode
            {
                Position = new Point(35, 35),
                Origin = new Point(10, 10),
                Attack = new LaserTierOne()
            });

            Turrets.Add(new TurretNode
            {
                Position = new Point(35, 147),
                Origin = new Point(10, 10),
                Attack = new PlasmaTierOne()
            });
        }

        public override string GetUnitImage()
        {
            return Content.FightScene.WarriorIcon;
        } 
    }
}
