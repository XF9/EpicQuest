﻿using EpicQuest.Engine.State;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Engine
{
    public abstract class Shader
    {
        public virtual void Update(IGameState state) { }
        public virtual void PreDraw(SpriteBatch batch) { }

        public virtual Texture2D GetPreDrawShaderResult() { return null; }
        public virtual Texture2D GetPostDrawShaderResult() { return null; }
    }
}
