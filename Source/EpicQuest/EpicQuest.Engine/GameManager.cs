﻿using System;
using System.Globalization;
using EpicQuest.Engine.ContentLoader;
using EpicQuest.Engine.State;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Engine
{
    public class GameManager
    {
        private static GameManager _instance;

        private Scene _currentScene;
        private ISceneFactory _sceneFactory;
        private GameState _gameState;
        public Screen Screen { get; private set; }
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private bool _exitRequested;
        private string _nextRequestedSzene;

        public event EventHandler OnExit;

        private bool IsInitialized { get; set; }

        public static ContentManager ContentManager { get; private set; }

        private GameManager(){ }

        public static GameManager GetInstance()
        {
            return _instance ?? (_instance = new GameManager());
        }

        public void Initialize(GraphicsDeviceManager graphics, SpriteBatch spriteBatch, ISceneFactory sceneFactory, Microsoft.Xna.Framework.Content.ContentManager xnaContentManager)
        {
            _sceneFactory = sceneFactory;
            _gameState = new GameState();
            _graphics = graphics;
            Screen = new Screen(_graphics, 1920, 1080);
            _spriteBatch = spriteBatch;
            ContentManager = new ContentManager(new XnaContentManager(xnaContentManager));

            SwitchScene("");
            IsInitialized = true;
        }

        public void Update(GameTime gameTime)
        {
            if (_exitRequested)
            {
                OnExit?.Invoke(this, System.EventArgs.Empty);
                return;
            }

            if (!string.IsNullOrEmpty(_nextRequestedSzene))
            {
                SwitchScene(_nextRequestedSzene);
                _nextRequestedSzene = "";
            }

            if (!IsInitialized)
                return;

            _gameState.Update(gameTime);
            _currentScene.UpdateScene(_gameState);
        }

        public void Draw()
        {
            if (!IsInitialized)
                return;

            Screen.PrepareRender();
            _currentScene.DrawScene(_spriteBatch);
        }

        public void RequestExit()
        {
            _exitRequested = true;
        }

        public void RequestScene(string sceneName)
        {
            _nextRequestedSzene = sceneName;
        }

        public CultureInfo GetCurrentCulture()
        {
            return new CultureInfo("de-DE");
        }

        private void SwitchScene(string nextSceneName)
        {
            ContentManager.UnloadAll();

            var nextScene = _sceneFactory.GetScene(nextSceneName, ContentManager);
            nextScene.LoadContent();
            _currentScene = nextScene;
        }

        public void KeyPressed(char c)
        {
            _gameState.TextInputState.KeyPressed(c);
        }
    }
}
