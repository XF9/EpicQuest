﻿using Microsoft.Xna.Framework;

namespace EpicQuest.Engine.Transformation
{
    public class TransformationDo
    {
        private Vector3 _translation = Vector3.Zero;
        private Vector3 _origin = Vector3.Zero;
        private float _rotation = 0;

        public Vector3 Translation
        {
            get => _translation;
            set
            {
                if (_translation != value)
                {
                    _translation = value;
                    HasChanged = true;
                }
            }
        }

        public Vector3 Origin
        {
            get => _origin;
            set
            {
                if (_origin != value)
                {
                    _origin = value;
                    HasChanged = true;
                }
            }
        }

        public float Rotation
        {
            get => _rotation;
            set
            {
                if (_rotation != value)
                {
                    _rotation = value;
                    HasChanged = true;
                }
            }
        }

        public bool HasChanged { get; set; }
    }
}
