﻿using Microsoft.Xna.Framework;

namespace EpicQuest.Engine.Transformation
{
    public class NodeTransformationDo
    {
        private readonly TransformationDo _parentTransformation;
        public TransformationDo LocalTransformation { get; }
        public TransformationDo GlobalTransformation { get; }

        public Matrix TransformationMatrix { get; private set; }

        public NodeTransformationDo()
        {
            _parentTransformation = new TransformationDo();
            LocalTransformation = new TransformationDo();
            GlobalTransformation = new TransformationDo();

            TransformationMatrix = Matrix.Identity;
        }

        public void Update(TransformationDo parentTransformation)
        {
            _parentTransformation.Translation = parentTransformation.Translation;
            _parentTransformation.Rotation = parentTransformation.Rotation;

            if (_parentTransformation.HasChanged || LocalTransformation.HasChanged)
            {
                var rotation = _parentTransformation.Rotation + LocalTransformation.Rotation;

                var globalTransformationMatrix = Matrix.Identity *
                                                 Matrix.CreateRotationZ(MathHelper.ToRadians(_parentTransformation.Rotation)) *
                                                 Matrix.CreateTranslation(_parentTransformation.Translation);

                globalTransformationMatrix = globalTransformationMatrix.MoveInDirection(LocalTransformation.Translation);
                var globalZero = Vector3.Transform(Vector3.Zero, globalTransformationMatrix);

                TransformationMatrix = Matrix.Identity *
                                       Matrix.CreateRotationZ(MathHelper.ToRadians(rotation)) *
                                       Matrix.CreateTranslation(globalZero);

                TransformationMatrix = TransformationMatrix.MoveInDirection(Vector3.Negate(LocalTransformation.Origin));

                GlobalTransformation.Translation = Vector3.Transform(Vector3.Zero, TransformationMatrix);
                GlobalTransformation.Rotation = rotation;

                _parentTransformation.HasChanged = false;
                LocalTransformation.HasChanged = false;
                GlobalTransformation.HasChanged = false;
            }
        }
    }
}
