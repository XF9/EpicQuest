﻿using EpicQuest.Engine.State;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Engine.SceneTree
{
    public class TextureNode : SceneTreeNode
    {
        public enum Border
        {
            Left,
            Top,
            Right,
            Bottom,
            AllSides
        }

        public Texture2D Texture { get; set; }
        public Color Color { get; set; }
        public Shader Shader { get; set; }

        private TextureNode _leftBorder;
        private TextureNode _topBorder;
        private TextureNode _rightBorder;
        private TextureNode _bottomBorder;
        private int _borderWidth;

        public TextureNode(Texture2D texture) : this(texture, texture.Width, texture.Height, Color.White) { }
        public TextureNode(Texture2D texture, Color color) : this(texture, texture.Width, texture.Height, color) { }

        public TextureNode(Texture2D texture, int width, int height) : this(texture, width, height, Color.White) { }
        public TextureNode(Texture2D texture, int width, int height, Color color) : this(texture, new Rectangle(0, 0, width, height), color) { }

        public TextureNode(Texture2D texture, Rectangle position) : this(texture, position, Color.White) {  }
        public TextureNode(Texture2D texture, Rectangle position, Color color)
        {
            Texture = texture;
            SetSize(position.Size);
            Color = color;
        }

        public void SetBorder(int width, Color color, Border borderOptions = Border.AllSides)
        {
            RemoveBorders();

            var texture = GameManager.ContentManager.GetAsset<Texture2D>(DefaultContent.Defaults.BlankTexture);
            _borderWidth = width;

            if (borderOptions == Border.Left || borderOptions == Border.AllSides)
            {
                _leftBorder = new TextureNode(texture, color);
                AddChild(_leftBorder);
            }

            if (borderOptions == Border.Top || borderOptions == Border.AllSides)
            {
                _topBorder = new TextureNode(texture, color);
                AddChild(_topBorder);
            }

            if (borderOptions == Border.Right || borderOptions == Border.AllSides)
            {
                _rightBorder = new TextureNode(texture, color);
                AddChild(_rightBorder);
            }

            if (borderOptions == Border.Bottom || borderOptions == Border.AllSides)
            {
                _bottomBorder = new TextureNode(texture, color);
                AddChild(_bottomBorder);
            }

            RefreshBorders();
        }

        public void RemoveBorders()
        {
            RemoveBorderElement(_leftBorder);
            RemoveBorderElement(_topBorder);
            RemoveBorderElement(_rightBorder);
            RemoveBorderElement(_bottomBorder);

            _leftBorder = null;
            _topBorder = null;
            _rightBorder = null;
            _bottomBorder = null;
        }

        public void SetBorderColor(Color color)
        {
            if(_leftBorder != null)
                _leftBorder.Color = color;

            if(_rightBorder != null)
                _rightBorder.Color = color;

            if(_topBorder != null)
                _topBorder.Color = color;

            if(_bottomBorder != null)
                _bottomBorder.Color = color;
        }

        private void RemoveBorderElement(TextureNode borderElement)
        {
            if (borderElement != null)
                RemoveChild(borderElement);
        }

        private void RefreshBorders()
        {
            _leftBorder?.SetPosition(0, 0);
            _leftBorder?.SetSize(_borderWidth, GetSize().Y);

            _rightBorder?.SetPosition(GetSize().X - _borderWidth, 0);
            _rightBorder?.SetSize(_borderWidth, GetSize().Y);

            _topBorder?.SetPosition(0, 0);
            _topBorder?.SetSize(GetSize().X, _borderWidth);

            _bottomBorder?.SetPosition(0, GetSize().Y - _borderWidth);
            _bottomBorder?.SetSize(GetSize().X, _borderWidth);
        }

        protected override void Update(IGameState state)
        {
            Shader?.Update(state);
        }

        protected override void PreDraw(SpriteBatch spriteBatch)
        {
            Shader?.PreDraw(spriteBatch);
        }

        protected override void Draw(SpriteBatch spriteBatch)
        {
            var screen = GameManager.GetInstance().Screen;

            screen.InitiateDraw(spriteBatch, this);

            DrawShader(Shader?.GetPreDrawShaderResult(), spriteBatch);
            spriteBatch.Draw(Texture, GetDimensions(), null, Color, 0.0f, new Vector2(0, 0), SpriteEffects.None, 0.0f);
            DrawShader(Shader?.GetPostDrawShaderResult(), spriteBatch);

            screen.EndDraw(spriteBatch);
        }

        private void DrawShader(Texture2D shaderResult, SpriteBatch spriteBatch)
        {
            if (shaderResult != null)
                spriteBatch.Draw(shaderResult, GetDimensions(), null, Color, 0.0f, new Vector2(0, 0), SpriteEffects.None, 0.0f);
        }

        public override SceneTreeNode SetSize(int x, int y)
        {
            base.SetSize(x, y);
            RefreshBorders();
            return this;
        }
    }
}
