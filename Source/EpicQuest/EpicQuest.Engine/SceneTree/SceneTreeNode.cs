﻿using System;
using System.Collections.Generic;
using System.Linq;
using EpicQuest.Engine.State;
using EpicQuest.Engine.Transformation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Engine.SceneTree
{
    public abstract class SceneTreeNode
    {
        private readonly List<SceneTreeNode> _children;
        private readonly List<SceneTreeNode> _childrenToAdd;
        private readonly List<SceneTreeNode> _childrenToRemove;

        private bool _visible;
        private bool _childrenVisible;

        public bool IsVisible => _visible;

        private Rectangle Dimensions { get; set; }
        private int? Layer { get; set; }

        private readonly List<MovementData> _movementQueue;

        private readonly NodeTransformationDo _transformationData;

        public Matrix TransformationMatrix => _transformationData.TransformationMatrix;
        public TransformationDo GlobalTransformation => _transformationData.GlobalTransformation;
        public TransformationDo Transformation => _transformationData.LocalTransformation;

        protected SceneTreeNode()
        {
            _children = new List<SceneTreeNode>();
            _childrenToAdd = new List<SceneTreeNode>();
            _childrenToRemove = new List<SceneTreeNode>();
            _movementQueue = new List<MovementData>();

            _visible = true;
            _childrenVisible = true;
            Dimensions = new Rectangle();
            _transformationData = new NodeTransformationDo();
        }

        protected virtual void PreDraw(SpriteBatch spriteBatch) { }
        protected virtual void Draw(SpriteBatch spriteBatch) { }
        protected virtual void Update(IGameState state){ }

        public void PrepareDrawNode(SpriteBatch spriteBatch)
        {
            if (_visible)
                PreDraw(spriteBatch);

            if (_childrenVisible)
                foreach (var child in _children.OrderBy(c => c.Layer))
                    child.PrepareDrawNode(spriteBatch);
        }

        public void DrawNode(SpriteBatch spriteBatch)
        {
            if(_visible)
                Draw(spriteBatch);

            if(_childrenVisible)
                foreach (var child in _children.OrderBy(c => c.Layer))
                    child.DrawNode(spriteBatch);
        }

        public void UpdateNode(IGameState state)
        {
            UpdateNode(state, new TransformationDo());
        }

        public void UpdateNode(IGameState state, TransformationDo parentTransformation)
        {
            ManageChilds();
            UpdateMovement(state.GameTime.ElapsedGameTime.TotalMilliseconds);
            _transformationData.Update(parentTransformation);

            Update(state);

            foreach (var child in _children)
                child.UpdateNode(state, GlobalTransformation);
        }

        private void ManageChilds()
        {
            _children.AddRange(_childrenToAdd);

            foreach (var child in _childrenToRemove)
                _children.Remove(child);

            _childrenToAdd.Clear();
            _childrenToRemove.Clear();
        }

        private void SetRotation(float degree)
        {
            Transformation.Rotation = degree;
        }

        private void SetTranslation(float x, float y)
        {
            Transformation.Translation = new Vector3(x, y, 0);
        }

        private void UpdateMovement(double passedMiliseconds)
        {
            var movementOrder = _movementQueue.FirstOrDefault();

            if (movementOrder == null)
                return;

            if (!movementOrder.InProgress)
            {
                if (movementOrder.Target.HasValue)
                    movementOrder.Offset = movementOrder.Target - Transformation.Translation;

                if (movementOrder.RotateTo.HasValue)
                    movementOrder.RotateBy = movementOrder.RotateTo - Transformation.Rotation;

                movementOrder.Origin = Transformation.Translation;
                movementOrder.OriginalRotation = Transformation.Rotation;
                movementOrder.InProgress = true;
            }

            movementOrder.PassedTime += passedMiliseconds;
            var overtime = 0d;
            if (movementOrder.PassedTime > movementOrder.TotalTime)
            {
                overtime = movementOrder.PassedTime - movementOrder.TotalTime;
                movementOrder.PassedTime = movementOrder.TotalTime;
            }

            var passedPercent = movementOrder.TotalTime <= 0 ? 1 : movementOrder.PassedTime / movementOrder.TotalTime;

            if (movementOrder.Origin.HasValue && movementOrder.Offset.HasValue)
            {
                var moveX = passedPercent * movementOrder.Offset.Value.X;
                var moveY = passedPercent * movementOrder.Offset.Value.Y;

                var movement = new Point((int)Math.Round(moveX + movementOrder.Origin.Value.X), (int)Math.Round(moveY + movementOrder.Origin.Value.Y));
                SetTranslation(movement.X, movement.Y);
            }

            if (movementOrder.OriginalRotation.HasValue && movementOrder.RotateBy.HasValue)
            {
                var rotate = passedPercent * movementOrder.RotateBy.Value;
                SetRotation(movementOrder.OriginalRotation.Value + (float)rotate);
            }

            if (movementOrder.PassedTime >= movementOrder.TotalTime)
            {
                _movementQueue.Remove(movementOrder);
                movementOrder.OnMovementDone?.Invoke();
            }

            if(overtime > 0)
                UpdateMovement(overtime);
        }

        public SceneTreeNode AddChild(SceneTreeNode child, int? layer = null)
        {
            var maxLayer = _children.Union(_childrenToAdd).Max(c => c.Layer);
            child.Layer = layer ?? child.Layer ?? maxLayer + 1;
            _childrenToAdd.Add(child);
            return this;
        }

        public SceneTreeNode RemoveChild(SceneTreeNode child)
        {
            _childrenToRemove.Add(child);
            return this;
        }

        public SceneTreeNode SetPosition(float x, float y, int timespan = 0, Action onMovementDone = null)
        {
            _movementQueue.Add(new MovementData
            {
                Target = new Vector3(x, y, 0),
                TotalTime = timespan,
                OnMovementDone = onMovementDone
            });

            return this;
        }

        public SceneTreeNode Move(float x, float y, int timespan = 0, Action onMovementDone = null)
        {
            _movementQueue.Add(new MovementData
            {
                Offset = new Vector3(x, y, 0),
                TotalTime = timespan,
                OnMovementDone = onMovementDone
            });

            return this;
        }

        public SceneTreeNode RotateTo(float angle, int timespan = 0, Action onMovementDone = null)
        {
            _movementQueue.Add(new MovementData
            {
                RotateTo = angle,
                TotalTime = timespan,
                OnMovementDone = onMovementDone
            });
            return this;
        }

        public SceneTreeNode RotateBy(float angle, int timespan = 0, Action onMovementDone = null)
        {
            _movementQueue.Add(new MovementData
            {
                RotateBy = angle,
                TotalTime = timespan,
                OnMovementDone = onMovementDone
            });
            return this;
        }

        public SceneTreeNode SetOrigin(int x, int y)
        {
            Transformation.Origin = new Vector3(x, y, 0);
            return this;
        }

        public Point GetSize()
        {
            return Dimensions.Size;
        }

        public Rectangle GetDimensions()
        {
            return Dimensions;
        }

        public virtual SceneTreeNode SetSize(int x, int y)
        {
            if (GetSize().X == 0 || GetSize().Y == 0)
            {
                Dimensions = new Rectangle(Dimensions.Location, new Point(x, y));
                return this;
            }

            var scaleX = x * 100f / GetSize().X;
            var scaleY = y * 100f / GetSize().Y;
            ScaleNode(scaleX, scaleY);
            return this;
        }

        public SceneTreeNode SetSize(Point size)
        {
            SetSize(size.X, size.Y);
            return this;
        }

        public SceneTreeNode Resize(int x, int y)
        {
            SetSize(GetSize().X + x, GetSize().Y + y);
            return this;
        }

        public SceneTreeNode Resize(Point size)
        {
            Resize(size.X, size.Y);
            return this;
        }

        public SceneTreeNode ScaleNode(float x, float y)
        {
            Dimensions = new Rectangle(Dimensions.X, Dimensions.Y, (int) (Dimensions.Width * x / 100), (int) (Dimensions.Height * y / 100));
            foreach (var child in _children)
                child.ScaleNode(x, y);

            return this;
        }

        public SceneTreeNode SetNodeVisibility(bool visible)
        {
            _visible = visible;
            return this;
        }

        public SceneTreeNode SetVisibility(bool visible)
        {
            _visible = visible;
            _childrenVisible = visible;
            return this;
        }

        private class MovementData
        {
            public Vector3? Target { get; set; }
            public Vector3? Offset { get; set; }

            public Vector3? Origin { get; set; }

            public float? OriginalRotation { get; set; }
            public float? RotateBy { get; set; }
            public float? RotateTo { get; set; }

            public double TotalTime { get; set; }
            public double PassedTime { get; set; }

            public Action OnMovementDone { get; set; }

            public bool InProgress { get; set; }
        }
    }
}
