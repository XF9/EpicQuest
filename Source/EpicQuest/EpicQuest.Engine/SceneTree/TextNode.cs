﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Engine.SceneTree
{
    public class TextNode : SceneTreeNode
    {
        private readonly SpriteFont _font;
        private string _text;

        private Color _color;

        public TextNode(SpriteFont font, string text) : this(font, text, Color.Black) { }
        public TextNode(SpriteFont font, string text, Color color)
        {
            _font = font;
            _text = text;
            _color = color;
            UpdateSize();
        }

        public void SetColor(Color color)
        {
            _color = color;
        }

        public void SetText(string text)
        {
            _text = text;
            UpdateSize();
        }

        private void UpdateSize()
        {
            SetSize(_font.MeasureString(_text ?? String.Empty).ToPoint());
        }

        protected override void Draw(SpriteBatch spriteBatch)
        {
            var screen = GameManager.GetInstance().Screen;
            screen.InitiateDraw(spriteBatch, this);

            spriteBatch.DrawString(_font, _text ?? "", Vector2.Zero, _color);

            screen.EndDraw(spriteBatch);
        }
    }
}
