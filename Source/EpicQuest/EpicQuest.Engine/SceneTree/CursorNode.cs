﻿using EpicQuest.Engine.State;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Engine.SceneTree
{
    public class CursorNode : SceneTreeNode
    {
        public CursorNode(){ }

        public CursorNode(Texture2D image)
        {
            AddChild(new TextureNode(image));
        }

        protected override void Update(IGameState state)
        {
            SetPosition(state.MouseState.Position.X, state.MouseState.Position.Y);
        }
    }
}
