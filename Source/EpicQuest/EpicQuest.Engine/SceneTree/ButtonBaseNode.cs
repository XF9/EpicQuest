﻿using System;
using EpicQuest.Engine.EventArgs;
using EpicQuest.Engine.State;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Engine.SceneTree
{
    public class ButtonBaseNode<T> : SceneTreeNode
    {
        protected TextureNode BackgroundNode;
        protected TextureNode ActiveBackgroundNode;

        public event EventHandler<PayloadEventArgs<T>> OnClick;
        public event EventHandler<PayloadEventArgs<T>> OnMouseEnter;
        public event EventHandler<PayloadEventArgs<T>> OnMouseLeft;

        public bool IsActive { get; private set; }
        private bool _wasActive;

        public T Payload { get; set; }

        public ButtonBaseNode()
        {
            OnMouseEnter += (sender, args) =>
            {
                ActiveBackgroundNode?.SetVisibility(true);
                BackgroundNode?.SetVisibility(ActiveBackgroundNode == null); // Set background to visible if no active background defined
            };

            OnMouseLeft += (sender, args) =>
            {
                BackgroundNode?.SetVisibility(true);
                ActiveBackgroundNode?.SetVisibility(false);
            };
        }

        public ButtonBaseNode<T> SetBackground(Texture2D texture)
        {
            return SetBackground(texture, Color.White);
        }

        public ButtonBaseNode<T> SetBackground(Texture2D texture, Color color)
        {
            RemoveBackground();
            BackgroundNode = new TextureNode(texture, GetSize().X, GetSize().Y , color);
            AddChild(BackgroundNode, 1);

            return this;
        }

        public ButtonBaseNode<T> RemoveBackground()
        {
            RemoveChild(BackgroundNode);
            BackgroundNode = null;
            return this;
        }

        public ButtonBaseNode<T> SetActiveBackground(Texture2D texture)
        {
            return SetActiveBackground(texture, Color.White);
        }

        public ButtonBaseNode<T> SetActiveBackground(Texture2D texture, Color color)
        {
            RemoveBackgroundHover();
            ActiveBackgroundNode = new TextureNode(texture, GetSize().X, GetSize().Y, color);
            ActiveBackgroundNode.SetVisibility(false);
            AddChild(ActiveBackgroundNode, 1);

            return this;
        }

        public ButtonBaseNode<T> RemoveBackgroundHover()
        {
            RemoveChild(ActiveBackgroundNode);
            ActiveBackgroundNode = null;

            return this;
        }

        protected override void Update(IGameState state)
        {
            _wasActive = IsActive;
            var mousePosition = Vector2.Transform(state.MouseState.Position, Matrix.Invert(TransformationMatrix));

            if (GetDimensions().Contains(mousePosition))
            {
                IsActive = true;

                if (!_wasActive)
                    OnMouseEnter?.Invoke(this, new PayloadEventArgs<T>(Payload));

                if (state.MouseState.LeftClick)
                    OnClick?.Invoke(this, new PayloadEventArgs<T>(Payload));
            }
            else
            {
                IsActive = false;

                if (_wasActive)
                    OnMouseLeft?.Invoke(this, new PayloadEventArgs<T>(Payload));
            }
        }
    }
}
