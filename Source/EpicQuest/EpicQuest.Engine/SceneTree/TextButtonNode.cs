﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Engine.SceneTree
{
    public class TextButtonNode : ButtonBaseNode<string>
    {
        private TextNode _text;
        private TextNode _activeText;

        public TextButtonNode(string text, SpriteFont font, Color textColor)
        {
            SetText(text, font, textColor);

            OnMouseEnter += (sender, args) =>
            {
                if (_activeText != null)
                {
                    _text?.SetVisibility(false);
                    _activeText.SetVisibility(true);
                }
            };

            OnMouseLeft += (sender, args) =>
            {
                if (_activeText != null)
                {
                    _text?.SetVisibility(true);
                    _activeText.SetVisibility(false);
                }
            };
        }

        public TextButtonNode SetText(string text, SpriteFont font, Color textColor)
        {
            _text = new TextNode(font, text, textColor);
            AddChild(_text, 2);
            UpdateTextPosition();

            return this;
        }

        public TextButtonNode SetActiveText(string text, SpriteFont font, Color textColor)
        {
            RemoveActiveText();
            _activeText = new TextNode(font, text, textColor);
            _activeText.SetVisibility(false);
            AddChild(_activeText, 2);
            UpdateTextPosition();

            return this;
        }

        public TextButtonNode RemoveActiveText()
        {
            RemoveChild(_activeText);
            _activeText = null;
            return this;
        }

        private void UpdateTextPosition()
        {
            Point textSize;
            int offsetX, offsetY;

            if (_text != null)
            {
                textSize = _text.GetSize();
                offsetX = (GetSize().X - textSize.X) / 2;
                offsetY = (GetSize().Y - textSize.Y) / 2;
                _text.SetPosition(offsetX, offsetY);
            }

            if (_activeText != null)
            {
                textSize = _activeText.GetSize();
                offsetX = (GetSize().X - textSize.X) / 2;
                offsetY = (GetSize().Y - textSize.Y) / 2;
                _activeText.SetPosition(offsetX, offsetY);
            }
        }

        public override SceneTreeNode SetSize(int x, int y)
        {
            base.SetSize(x, y);
            UpdateTextPosition();
            return this;
        }
    }
}
