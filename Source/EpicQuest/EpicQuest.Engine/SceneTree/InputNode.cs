﻿using EpicQuest.Engine.State;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Engine.SceneTree
{
    public class InputNode : SceneTreeNode
    {
        private TextureNode _background;
        private readonly TextNode _textNode;

        private bool _hasFocus;
        private string _text;

        public InputNode(int width, int heigth, Texture2D background, SpriteFont font, Color textColor) : this(new Rectangle(0, 0, width, heigth), background, font, textColor) { }
        public InputNode(Rectangle dimensions, Texture2D background, SpriteFont font, Color textColor)
        {
            SetSize(dimensions.Width, dimensions.Height);
            _background = new TextureNode(background, dimensions);
            _textNode = new TextNode(font, "a", textColor);

            AddChild(_background, 1);
            AddChild(_textNode, 2);
        }

        public InputNode SetFocus(bool focus)
        {
            _hasFocus = focus;
            return this;
        }

        public InputNode SetBackground(Texture2D texture, Color color)
        {
            RemoveBackground();
            _background = new TextureNode(texture, color);
            _background.SetSize(GetSize());
            AddChild(_background, 1);
            return this;
        }

        public InputNode RemoveBackground()
        {
            RemoveChild(_background);
            _background = null;
            return this;
        }

        protected override void Update(IGameState state)
        {
            if (state.MouseState.LeftClick)
                _hasFocus = GetDimensions().Contains(state.MouseState.Position);

            if (_hasFocus)
            {
                _text += state.TextInputState.Input;

                if (state.TextInputState.IsKeyPressed(TextInputState.ControlKeys.Backspace))
                    _text = _text.Length > 0 ? _text.Substring(0, _text.Length - 1) : _text;

                _textNode.SetText(_text + "_");
            }
            else
            {
                _textNode.SetText(_text);
            }
        }
    }
}
