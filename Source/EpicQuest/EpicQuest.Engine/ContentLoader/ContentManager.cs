﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace EpicQuest.Engine.ContentLoader
{
    public class ContentManager
    {
        private readonly IXnaContentManager _xnaContentManager;

        private readonly Dictionary<System.Type, Dictionary<string, object>> _loadedAssets;
        private readonly Dictionary<System.Type, List<string>> _queuedAssets;

        public ContentManager(IXnaContentManager contentManager)
        {
            _xnaContentManager = contentManager;
            _loadedAssets = new Dictionary<Type, Dictionary<string, object>>();
            _queuedAssets = new Dictionary<Type, List<string>>();
        }

        public ContentManager QueueAssetLoad<T>(string path)
        {
            if(!_queuedAssets.ContainsKey(typeof(T)))
                _queuedAssets[typeof(T)] = new List<string>();
            _queuedAssets[typeof(T)].Add(path);
            return this;
        }

        public ContentManager LoadAssets()
        {
            foreach (var type in _queuedAssets.Keys)
            {
                _loadedAssets[type] = new Dictionary<string, object>();
                var methodReference = type.GetInterfaces().Contains(typeof(IXmlFile))
                    ? typeof(XmlHandler).GetMethod(nameof(XmlHandler.Load))?.MakeGenericMethod(type)
                    : typeof(IXnaContentManager).GetMethod(nameof(IXnaContentManager.Load))?.MakeGenericMethod(type);

                foreach (var path in _queuedAssets[type])
                {
                    var asset = methodReference?.Invoke(_xnaContentManager, new object[] { path });
                    _loadedAssets[type].Add(path, asset);
                }

            }

            _queuedAssets.Clear();

            return this;
        }

        public T GetAsset<T>(string path)
        {
            var asset = _loadedAssets[typeof(T)][path];
            return (T) asset;
        }

        public ContentManager UnloadAll()
        {
            _xnaContentManager.Unload();
            _loadedAssets.Clear();

            return this;
        }
    }
}