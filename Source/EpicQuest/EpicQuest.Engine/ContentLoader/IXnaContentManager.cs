﻿namespace EpicQuest.Engine.ContentLoader
{
    public interface IXnaContentManager
    {
        T Load<T>(string path);
        void Unload();
    }
}
