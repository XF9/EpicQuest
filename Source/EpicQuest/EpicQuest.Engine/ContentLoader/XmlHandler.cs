﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace EpicQuest.Engine.ContentLoader
{
    class XmlHandler
    {
        public static T Load<T>(string path) where T : IXmlFile
        {
            var result = default(T);
            if (File.Exists(path))
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(path);
                string xmlString = xmlDocument.OuterXml;

                using (StringReader read = new StringReader(xmlString))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(T));
                    using (XmlReader reader = new XmlTextReader(read))
                    {
                        result = (T)serializer.Deserialize(reader);
                        reader.Close();
                    }
                    read.Close();
                }
            }

            return result;
        }

        public static void Save<T>(string filename, T toBeSaved)
        {
            var xmlDocument = new XmlDocument();
            var serializer = new XmlSerializer(typeof(T));

            using (MemoryStream stream = new MemoryStream())
            {
                serializer.Serialize(stream, toBeSaved);
                stream.Position = 0;
                xmlDocument.Load(stream);
                xmlDocument.Save(filename);
                stream.Close();
            }
        }
    }
}
