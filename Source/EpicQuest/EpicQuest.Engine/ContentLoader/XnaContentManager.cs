﻿namespace EpicQuest.Engine.ContentLoader
{
    public class XnaContentManager : IXnaContentManager
    {
        private readonly Microsoft.Xna.Framework.Content.ContentManager _manager;

        public XnaContentManager(Microsoft.Xna.Framework.Content.ContentManager manager)
        {
            _manager = manager;
        }

        public T Load<T>(string path)
        {
            return _manager.Load<T>(path);
        }

        public void Unload()
        {
            _manager.Unload();
        }
    }
}
