﻿using System;
using EpicQuest.Engine.ContentLoader;

namespace EpicQuest.Engine
{
    public interface ISceneFactory
    {
        Scene GetScene(String scenetype, ContentManager contentManager);
    }
}
