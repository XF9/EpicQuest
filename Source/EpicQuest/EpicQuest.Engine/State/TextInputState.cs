﻿using System;
using System.Collections.Generic;

namespace EpicQuest.Engine.State
{
    public class TextInputState
    {
        public enum ControlKeys
        {
            Backspace,
            Enter
        }

        public string Input { get; private set; }

        private readonly List<ControlKeys> _pressedControllKeys;
        private readonly List<ControlKeys> _controllKeyBuffer;

        private string _currentInput;

        public TextInputState()
        {
            _pressedControllKeys = new List<ControlKeys>();
            _controllKeyBuffer = new List<ControlKeys>();
        }

        public void Update()
        {
            Input = _currentInput;
            _currentInput = "";

            _controllKeyBuffer.Clear();
            _controllKeyBuffer.AddRange(_pressedControllKeys);
            _pressedControllKeys.Clear();
        }

        public void KeyPressed(char key)
        {
            if(!Char.IsControl(key))
                _currentInput += key;
            else
                switch (key)
                {
                    case '\b':
                        _pressedControllKeys.Add(ControlKeys.Backspace);
                        break;
                    case '\r':
                        _pressedControllKeys.Add(ControlKeys.Enter);
                        break;
                }
        }

        public bool IsKeyPressed(ControlKeys key)
        {
            return _controllKeyBuffer.Contains(key);
        }
    }
}
