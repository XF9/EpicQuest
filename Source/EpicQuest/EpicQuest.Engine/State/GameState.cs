﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace EpicQuest.Engine.State
{
    public class GameState : IGameState
    {
        public MouseState MouseState { get; private set; }
        public KeyboardState KeyboardState { get; private set; }
        public TextInputState TextInputState { get; private set; }

        public GameTime GameTime { get; private set; }

        public GameState()
        {
            MouseState = new MouseState();
            KeyboardState = new KeyboardState();
            TextInputState = new TextInputState();
        }

        public void Update(GameTime gameTime)
        {
            MouseState.Update(Mouse.GetState());
            KeyboardState.Update(Keyboard.GetState());
            TextInputState.Update();
            GameTime = gameTime;
        }
    }
}
