﻿using Microsoft.Xna.Framework.Input;

namespace EpicQuest.Engine.State
{
    public class KeyboardState
    {
        private Microsoft.Xna.Framework.Input.KeyboardState _currentState;

        public void Update(Microsoft.Xna.Framework.Input.KeyboardState keyboarState)
        {
            _currentState = keyboarState;
        }

        public bool IsKeyPressed(Keys key)
        {
            return _currentState.IsKeyDown(key);
        }
    }
}
