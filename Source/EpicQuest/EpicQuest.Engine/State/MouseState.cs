﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace EpicQuest.Engine.State
{
    public class MouseState
    {
        public Vector2 Position { get; protected set; }

        public bool LeftClick { get; protected set; }

        private ButtonState _lastLeftMouseButtonState = ButtonState.Released;

        public void Update(Microsoft.Xna.Framework.Input.MouseState mouse)
        {
            UpdateMousePosition(mouse);
            UpdateButtonClick(mouse);
        }

        private void UpdateMousePosition(Microsoft.Xna.Framework.Input.MouseState mouse)
        {
            var screen = GameManager.GetInstance().Screen;

            if (screen == null)
                return;

            var absoluteMousePosition = screen.ToAbsolutePosition(mouse.Position.ToVector2());
            var invertedScaleMatrix = Matrix.Invert(screen.ScaleMatrix);

            var position = Vector2.Transform(absoluteMousePosition, invertedScaleMatrix);

            position.X = position.X < 0
                ? 0
                : position.X > screen.InternalSize.X
                    ? screen.InternalSize.X
                    : position.X;

            position.Y = position.Y < 0
                ? 0
                : position.Y > screen.InternalSize.Y
                    ? screen.InternalSize.Y
                    : position.Y;

            Position = position;
        }

        private void UpdateButtonClick(Microsoft.Xna.Framework.Input.MouseState mouse)
        {
            LeftClick = mouse.LeftButton == ButtonState.Released && _lastLeftMouseButtonState == ButtonState.Pressed;
            _lastLeftMouseButtonState = mouse.LeftButton;
        }
    }
}
