﻿using Microsoft.Xna.Framework;

namespace EpicQuest.Engine.State
{
    public interface IGameState
    {
        GameTime GameTime { get; }
        KeyboardState KeyboardState { get; }
        MouseState MouseState { get; }
        TextInputState TextInputState { get; }

        void Update(GameTime gameTime);
    }
}