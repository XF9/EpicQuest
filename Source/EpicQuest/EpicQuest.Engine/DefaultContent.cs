﻿namespace EpicQuest.Engine
{
    public class DefaultContent
    {
        public static class Defaults
        {
            public const string Cursor = "Ui/Cursor";
            public const string BlankTexture = "Test/square";
        }
    }
}
