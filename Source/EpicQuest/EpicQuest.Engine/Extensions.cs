﻿using System;
using Microsoft.Xna.Framework;

namespace EpicQuest.Engine
{
    static class Extensions
    {
        public static Matrix MoveInDirection(this Matrix transformationMatrix, Vector3 direction)
        {
            transformationMatrix.Translation += Vector3.Normalize(transformationMatrix.Right) * direction.X;
            transformationMatrix.Translation += Vector3.Normalize(transformationMatrix.Up) * direction.Y;

            return transformationMatrix;
        }

        public static Point ToPoint(this Vector3 source)
        {
            return new Point((int) source.X, (int) source.Y);
        }
    }
}
