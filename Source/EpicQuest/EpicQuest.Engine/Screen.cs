﻿using System;
using System.Collections.Generic;
using EpicQuest.Engine.SceneTree;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Engine
{
    public class Screen
    {
        private readonly GraphicsDeviceManager _device;


        public Point WindowSize { get; private set; }
        public Point InternalSize { get; private set; }
        private Point _renderSize;
        private Point _renderOffset;

        public bool Fullscreen { get; private set; }
        public bool VSync { get; private set; }

        public Matrix ScaleMatrix { get; private set; }

        public GraphicsDevice GraphicsDevice
        {
            get { return _device.GraphicsDevice; }
        }

        public Screen(GraphicsDeviceManager device, int renderWidth, int renderHeight)
        {
            _device = device;
            InternalSize = new Point(renderWidth, renderHeight);
            SetResolution(renderWidth, renderHeight, false);
        }

        public Screen SetResolution(int width, int heigth, bool fullscreen)
        {
            WindowSize = new Point(width, heigth);
            Fullscreen = fullscreen;
            ApplyChanges();
            return this;
        }

        public Screen SetVSync(bool active)
        {
            VSync = active;
            _device.SynchronizeWithVerticalRetrace = VSync;
            _device.ApplyChanges();
            return this;
        }

        private Screen ApplyChanges()
        {
            var scaleX = (float)WindowSize.X / InternalSize.X;
            var scaleY = (float)WindowSize.Y / InternalSize.Y;
            var scale = Math.Min(scaleX, scaleY);

            _renderSize = new Point((int)(InternalSize.X * scale), (int)(InternalSize.Y * scale));
            _renderOffset = new Point((int)((double)WindowSize.X / 2 - (double)_renderSize.X / 2), (int)((double)WindowSize.Y / 2 - (double)_renderSize.Y / 2));

            ScaleMatrix = Matrix.CreateScale(scale, scale, 1);
            _device.IsFullScreen = Fullscreen;
            _device.PreferredBackBufferWidth = WindowSize.X;
            _device.PreferredBackBufferHeight = WindowSize.Y;
            _device.ApplyChanges();

            return this;
        }

        public Screen InitiateDraw(SpriteBatch batch, SceneTreeNode node, bool useScaleMatrix = true)
        {
            var transformation = node?.TransformationMatrix ?? Matrix.Identity;

            batch.Begin(SpriteSortMode.Immediate,
                BlendState.NonPremultiplied,
                SamplerState.PointClamp,
                DepthStencilState.Default,
                RasterizerState.CullNone,
                null,
                useScaleMatrix ? transformation * ScaleMatrix : transformation
                );

            return this;
        }

        public Screen EndDraw(SpriteBatch batch)
        {
            batch.End();
            return this;
        }

        public Screen PrepareRender()
        {
            var viewPortWindow = new Viewport
            {
                X = 0,
                Y = 0,
                Width = WindowSize.X,
                Height = WindowSize.Y
            };
            GraphicsDevice.Viewport = viewPortWindow;
            GraphicsDevice.Clear(Color.Black);

            var viewPortGame = new Viewport
            {
                X = _renderOffset.X,
                Y = _renderOffset.Y,
                Width = _renderSize.X,
                Height = _renderSize.Y,
                MinDepth = 0,
                MaxDepth = 1
            };

            GraphicsDevice.Viewport = viewPortGame;
            GraphicsDevice.Clear(Color.Black);

            return this;
        }

        public Vector2 ToAbsolutePosition(Vector2 position)
        {
            return new Vector2(position.X - _renderOffset.X, position.Y - _renderOffset.Y);
        }
    }
}
