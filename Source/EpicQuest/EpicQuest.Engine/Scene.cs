﻿using System.Threading;
using EpicQuest.Engine.ContentLoader;
using EpicQuest.Engine.SceneTree;
using EpicQuest.Engine.State;
using EpicQuest.Engine.Transformation;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Engine
{
    public abstract class Scene
    {
        private readonly SceneTreeNode _rootSceneTreeNode;
        protected readonly SceneTreeNode SceneTree;

        private readonly ContentManager _contentManager;

        private readonly TransformationDo _emptyTransformation;

        protected virtual void InitializeSceneTree() { }
        protected virtual void Update(IGameState state){ }

        protected string CursorTexture;

        protected Scene(ContentManager contentManager)
        {
            _contentManager = contentManager;
            _emptyTransformation = new TransformationDo();

            SceneTree = new EmptyNode();
            _rootSceneTreeNode = new EmptyNode();

            Thread.CurrentThread.CurrentCulture = GameManager.GetInstance().GetCurrentCulture();
            Thread.CurrentThread.CurrentUICulture = GameManager.GetInstance().GetCurrentCulture();
        }

        public void DrawScene(SpriteBatch spriteBatch)
        {
            _rootSceneTreeNode.PrepareDrawNode(spriteBatch);
            _rootSceneTreeNode.DrawNode(spriteBatch);
        }

        public void UpdateScene(GameState state)
        {
            Update(state);
            _rootSceneTreeNode.UpdateNode(state, _emptyTransformation);
        }

        public void LoadContent()
        {
            _contentManager.QueueAssetLoad<Texture2D>(CursorTexture);
            _contentManager.LoadAssets();

            _rootSceneTreeNode.AddChild(SceneTree);
            _rootSceneTreeNode.AddChild(new CursorNode(GameManager.ContentManager.GetAsset<Texture2D>(CursorTexture)));

            InitializeSceneTree();
        }
    }
}
