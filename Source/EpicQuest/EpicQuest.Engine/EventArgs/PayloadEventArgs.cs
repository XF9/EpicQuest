﻿namespace EpicQuest.Engine.EventArgs
{
    public class PayloadEventArgs<T> : System.EventArgs
    {
        public T Payload { get; set; }

        public PayloadEventArgs(T payload)
        {
            Payload = payload;
        }
    }
}
