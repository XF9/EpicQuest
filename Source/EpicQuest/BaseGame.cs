﻿using EpicQuest.Engine;
using EpicQuest.Game.Helper;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest
{
    public class BaseGame : Microsoft.Xna.Framework.Game
    {
        private readonly GraphicsDeviceManager _graphics;

        public BaseGame()
        {
            Content.RootDirectory = "Content";
            _graphics = new GraphicsDeviceManager(this);
        }

        protected override void LoadContent()
        {
            var spriteBatch = new SpriteBatch(GraphicsDevice);
            var sceneFactory = new SceneFactory();

            GameManager.GetInstance().Initialize(_graphics, spriteBatch, sceneFactory, Content);
            GameManager.GetInstance().OnExit += (sender, args) => Exit();

            Window.TextInput += (sender, args) => GameManager.GetInstance().KeyPressed(args.Character);

            base.LoadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            GameManager.GetInstance().Update(gameTime);
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GameManager.GetInstance().Draw();
            base.Draw(gameTime);
        }
    }
}
