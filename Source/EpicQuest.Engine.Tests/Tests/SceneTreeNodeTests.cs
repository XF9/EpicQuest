﻿using EpicQuest.Engine.SceneTree;
using EpicQuest.Engine.Test.Fakes;
using Machine.Specifications;

namespace EpicQuest.Engine.Test.Tests
{
    [Subject(nameof(SceneTreeNode))]
    class when_removing_child_and_updating_parent_node : whith_a_child_node
    {
        private Establish context = EstablishContext;

        private Because of = () =>
        {
            Parent.RemoveChild(Child);
            Parent.UpdateNode(CurrentGameState);
        };

        private It should_not_have_updated_child_node = () => Child.HasBeenUpdated.ShouldBeFalse();
    }

    [Subject(nameof(SceneTreeNode))]
    class when_updating_parent_node : whith_a_child_node
    {
        private Establish context = EstablishContext;

        private Because of = () =>
        {
            Parent.UpdateNode(CurrentGameState);
        };

        private It should_have_updated_child_node = () => Child.HasBeenUpdated.ShouldBeTrue();
    }

    [Subject(nameof(SceneTreeNode))]
    class when_drawing_parent_node : whith_a_child_node
    {
        private Establish context = EstablishContext;

        private Because of = () =>
        {
            Parent.DrawNode(null);
        };

        private It should_have_drawn_child_node = () => Child.HasBeenDrawn.ShouldBeTrue();
    }

    [Subject(nameof(SceneTreeNode))]
    class when_preparing_drawing_parent_node : whith_a_child_node
    {
        private Establish context = EstablishContext;

        private Because of = () =>
        {
            Parent.PrepareDrawNode(null);
        };

        private It should_have_drawn_child_node = () => Child.HasPreparedDraw.ShouldBeTrue();
    }

    [Subject(nameof(SceneTreeNode))]
    class when_setting_and_moving_node : whith_a_child_node
    {
        private Establish context = EstablishContext;

        private Because of = () =>
        {
            Parent.SetPosition(20, 30);
            Parent.Move(15, 15);
            Parent.UpdateNode(CurrentGameState);
        };

        private It should_have_moved_node_horizontally = () => Parent.GlobalTransformation.Translation.X.ShouldEqual(35);
        private It should_have_moved_node_vertical = () => Parent.GlobalTransformation.Translation.Y.ShouldEqual(45);
    }

    [Subject(nameof(SceneTreeNode))]
    class when_setting_and_rotating_node : whith_a_child_node
    {
        private Establish context = EstablishContext;

        private Because of = () =>
        {
            Parent.RotateTo(45);
            Parent.RotateBy(10);
            Parent.UpdateNode(CurrentGameState);
        };

        private It should_have_set_rotation = () => Parent.GlobalTransformation.Rotation.ShouldEqual(55);
    }

    [Subject(nameof(SceneTreeNode))]
    class when_transforming_parent_node : whith_a_child_node
    {
        private Establish context = EstablishContext;

        private Because of = () =>
        {
            Parent.SetPosition(20, 30);
            Parent.RotateTo(90);
            Parent.UpdateNode(CurrentGameState);
        };

        private It should_have_updated_child_node = () => Child.HasBeenUpdated.ShouldBeTrue();
        private It should_have_moved_child_node_horizontally = () => Child.GlobalTransformation.Translation.X.ShouldEqual(20);
        private It should_have_moved_child_node_vertical = () => Child.GlobalTransformation.Translation.Y.ShouldEqual(30);
        private It should_have_rotated_child_node_vertical = () => Child.GlobalTransformation.Rotation.ShouldEqual(90);
    }

    [Subject(nameof(SceneTreeNode))]
    class when_transforming_parent_and_child_node : whith_a_child_node
    {
        private Establish context = EstablishContext;

        private Because of = () =>
        {
            Parent.SetPosition(20, 30);
            Parent.RotateTo(90);

            Child.SetPosition(15, -10);
            Child.RotateTo(20);

            Parent.UpdateNode(CurrentGameState);
        };

        private It should_have_updated_child_node = () => Child.HasBeenUpdated.ShouldBeTrue();
        private It should_have_moved_child_node_horizontally = () => Child.GlobalTransformation.Translation.X.ShouldEqual(30);
        private It should_have_moved_child_node_vertical = () => Child.GlobalTransformation.Translation.Y.ShouldEqual(45);
        private It should_have_rotated_child_node_vertical = () => Child.GlobalTransformation.Rotation.ShouldEqual(110);
    }

    [Subject(nameof(SceneTreeNode))]
    class when_transforming_parent_and_child_node_with_half_of_the_needed_time : whith_a_child_node
    {
        private Establish context = EstablishContext;

        private Because of = () =>
        {
            Parent.SetPosition(20, 20, ElapsedMilliseconds * 2);
            Parent.UpdateNode(CurrentGameState);
        };

        private It should_have_moved_child_node_horizontally = () => Child.GlobalTransformation.Translation.X.ShouldEqual(10);
        private It should_have_moved_child_node_vertical = () => Child.GlobalTransformation.Translation.Y.ShouldEqual(10);
    }

    [Subject(nameof(SceneTreeNode))]
    class when_transforming_parent_and_child_node_two_tinmes_with_half_of_the_needed_time_for_the_second_one : whith_a_child_node
    {
        private Establish context = EstablishContext;

        private Because of = () =>
        {
            Parent.Move(20, 20, ElapsedMilliseconds / 2);
            Parent.Move(30, 30, ElapsedMilliseconds);
            Parent.UpdateNode(CurrentGameState);
        };

        private It should_have_moved_child_node_horizontally = () => Child.GlobalTransformation.Translation.X.ShouldEqual(20 + 15);
        private It should_have_moved_child_node_vertical = () => Child.GlobalTransformation.Translation.Y.ShouldEqual(20 + 15);
    }

    public class whith_a_child_node : TestBase
    {
        protected static FakeSceneTreeNode Parent;
        protected static FakeSceneTreeNode Child;

        protected new static void EstablishContext()
        {
            TestBase.EstablishContext();

            Parent = new FakeSceneTreeNode();
            Child = new FakeSceneTreeNode();

            Parent.AddChild(Child);
            Parent.UpdateNode(CurrentGameState);

            Parent.ResetTest();
            Child.ResetTest();
        }
    }
}
