﻿using System;
using EpicQuest.Engine.ContentLoader;
using EpicQuest.Engine.Test.Fakes;
using Machine.Specifications;

namespace EpicQuest.Engine.Test.Tests
{
    class ContentManagerTests
    {
        [Subject(nameof(ContentManager))]
        class when_loading_content : with_contentManager
        {
            private Establish context = EstablishContext;

            private Because of = () =>
            {
                ContentManager
                    .QueueAssetLoad<Object>(ObjectPath)
                    .QueueAssetLoad<string>(StringPath)
                    .QueueAssetLoad<int>(IntPath)
                    .LoadAssets();
            };

            private It should_have_some_object = () => ContentManager.GetAsset<Object>(ObjectPath).ShouldNotBeNull();
            private It should_return_the_stringpath = () => ContentManager.GetAsset<string>(StringPath).ShouldEqual(StringPath);
            private It should_return_the_stringlength = () => ContentManager.GetAsset<int>(IntPath).ShouldEqual(IntPath.Length);
        }

        public class with_contentManager : TestBase
        {
            protected static ContentManager ContentManager;

            protected const string ObjectPath = "Object";
            protected const string StringPath = "StringPath";
            protected const string IntPath = "IntPath";

            protected new static void EstablishContext()
            {
                TestBase.EstablishContext();

                ContentManager = new ContentManager(new FakeXnaContentManager());
            }
        }
    }
}
