﻿using System;
using EpicQuest.Engine.State;
using EpicQuest.Engine.Test.Fakes;
using FakeItEasy;
using Microsoft.Xna.Framework;

namespace EpicQuest.Engine.Test.Tests
{
    public abstract class TestBase
    {
        protected static IGameState CurrentGameState;
        protected static FakeMouseState CurrentMouseState;

        protected static int ElapsedMilliseconds = 10;
        protected static int TotalElapsedMilliseconds = 10;

        protected static void EstablishContext()
        {
            CurrentGameState = A.Fake<IGameState>();

            CurrentMouseState = new FakeMouseState();

            A.CallTo(() => CurrentGameState.MouseState).Returns(CurrentMouseState);
            A.CallTo(() => CurrentGameState.GameTime).Returns(new GameTime(new TimeSpan(0, 0, 0, 0, TotalElapsedMilliseconds), new TimeSpan(0, 0, 0, 0, ElapsedMilliseconds)));
        }
    }
}
