﻿using EpicQuest.Engine.SceneTree;
using Machine.Specifications;
using Microsoft.Xna.Framework;

namespace EpicQuest.Engine.Test.Tests
{
    class CursorTests
    {
        [Subject(nameof(CursorNode))]
        class when_moving_mouse : TestBase
        {
            protected static CursorNode Node;

            private Establish context = () =>
            {
                TestBase.EstablishContext();
                Node = new CursorNode();
            };

            private Because of = () =>
            {
                CurrentMouseState.SetPosition(new Vector2(50, 100));

                // We need two updates because the position is updated before the node update takes place, which sets the new position
                // this order is required, so that Update allways get the current state
                Node.UpdateNode(CurrentGameState);
                Node.UpdateNode(CurrentGameState);
            };

            private It should_have_moved_cursor_horizontally = () => Node.Transformation.Translation.X.ShouldEqual(50);
            private It should_have_moved_cursor_vertically = () => Node.Transformation.Translation.Y.ShouldEqual(100);
        }
    }
}
