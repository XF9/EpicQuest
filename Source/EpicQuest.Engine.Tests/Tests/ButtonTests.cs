﻿using EpicQuest.Engine.SceneTree;
using EpicQuest.Engine.Test.Fakes;
using Machine.Specifications;
using Microsoft.Xna.Framework;

namespace EpicQuest.Engine.Test.Tests
{
    class ButtonTests
    {
        [Subject(nameof(ButtonBaseNode<string>))]
        class when_having_a_button_node_and_left_clicking_somewhere_else : with_button_node
        {
            private Establish context = EstablishContext;

            private It should_not_have_hovered_state = () => ButtonNode.IsActive.ShouldBeFalse();
            private It should_have_visible_background = () => ButtonNode.BackgroundVisible.ShouldBeTrue();
            private It should_not_have_visible_active_background = () => ButtonNode.ActiveBackgroundVisible.ShouldBeFalse();
            private It should_not_have_fired_clickevent = () => ClickResult.ShouldBeNull();
        }

        [Subject(nameof(ButtonBaseNode<string>))]
        class when_having_a_hovered_button_node_and_clicking_it : with_button_node
        {
            private Establish context = EstablishContext;

            private Because of = () =>
            {
                CurrentMouseState.SetPosition(ButtonNode.TransformationMatrix.Translation);
                CurrentMouseState.SetLeftClick(true);
                ButtonNode.UpdateNode(CurrentGameState);
            };

            private It should_have_hovered_state = () => ButtonNode.IsActive.ShouldBeTrue();
            private It should_not_have_visible_background = () => ButtonNode.BackgroundVisible.ShouldBeFalse();
            private It should_have_visible_active_background = () => ButtonNode.ActiveBackgroundVisible.ShouldBeTrue();
            private It should_have_fired_clickevent = () => ClickResult.ShouldEqual(ButtonPayload);
        }

        [Subject(nameof(ButtonBaseNode<string>))]
        class when_hovering_and_leaving_button_node_and_left_click : with_button_node
        {
            private Establish context = EstablishContext;

            private Because of = () =>
            {
                CurrentMouseState.SetPosition(ButtonNode.TransformationMatrix.Translation);
                ButtonNode.UpdateNode(CurrentGameState);

                CurrentMouseState.SetPosition(ButtonNode.TransformationMatrix.Translation + new Vector3(100));
                CurrentMouseState.SetLeftClick(false);
                ButtonNode.UpdateNode(CurrentGameState);
            };

            private It should_not_have_hovered_state = () => ButtonNode.IsActive.ShouldBeFalse();
            private It should_have_visible_background = () => ButtonNode.BackgroundVisible.ShouldBeTrue();
            private It should_not_have_visible_active_background = () => ButtonNode.ActiveBackgroundVisible.ShouldBeFalse();
            private It should_not_have_fired_clickevent = () => ClickResult.ShouldBeNull();
        }

        public class with_button_node : TestBase
        {
            protected static FakeButtonNode ButtonNode;
            protected static string ButtonPayload = "ButtonPayload";

            protected static string ClickResult;

            protected new static void EstablishContext()
            {
                TestBase.EstablishContext();

                ButtonNode = new FakeButtonNode(ButtonPayload);

                ButtonNode.SetSize(10, 10);
                ButtonNode.SetPosition(10, 10);

                ButtonNode.SetBackground(null);
                ButtonNode.SetActiveBackground(null);

                ButtonNode.OnClick += (sender, args) => { ClickResult = args.Payload; };

                ButtonNode.UpdateNode(CurrentGameState);

                ClickResult = null;
            }
        }
    }
}
