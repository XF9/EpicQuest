﻿using EpicQuest.Engine.State;
using Microsoft.Xna.Framework;

namespace EpicQuest.Engine.Test.Fakes
{
    public class FakeMouseState : MouseState
    {
        public void SetPosition(Vector2 position)
        {
            Position = position;
        }

        public void SetPosition(Vector3 position)
        {
            Position = new Vector2(position.X, position.Y);
        }

        public void SetLeftClick(bool click)
        {
            LeftClick = click;
        }
    }
}
