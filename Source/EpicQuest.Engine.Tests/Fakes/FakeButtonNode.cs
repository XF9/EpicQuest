﻿using EpicQuest.Engine.SceneTree;

namespace EpicQuest.Engine.Test.Fakes
{
    public class FakeButtonNode : ButtonBaseNode<string>
    {
        public bool BackgroundVisible => BackgroundNode?.IsVisible ?? false;
        public bool ActiveBackgroundVisible => ActiveBackgroundNode?.IsVisible ?? false;

        public FakeButtonNode(string payload)
        {
            Payload = payload;
        }
    }
}
