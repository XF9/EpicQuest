﻿using System;
using System.Collections.Generic;
using EpicQuest.Engine.ContentLoader;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Engine.Test.Fakes
{
    class FakeXnaContentManager : IXnaContentManager
    {

        public T Load<T>(string path)
        {
            object result = null;

            if (typeof(T) == typeof(Object))
                result = new Object();

            if (typeof(T) == typeof(string))
                result = path;

            if (typeof(T) == typeof(int))
                result = path.Length;

            return (T) result;
        }

        public void Unload()
        {
            throw new System.NotImplementedException();
        }
    }
}
