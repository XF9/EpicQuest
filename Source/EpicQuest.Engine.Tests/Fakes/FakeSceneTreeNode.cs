﻿using EpicQuest.Engine.SceneTree;
using EpicQuest.Engine.State;
using Microsoft.Xna.Framework.Graphics;

namespace EpicQuest.Engine.Test.Fakes
{
    public class FakeSceneTreeNode : SceneTreeNode
    {
        public bool HasBeenUpdated { get; set; }
        public bool HasBeenDrawn { get; set; }
        public bool HasPreparedDraw { get; set; }

        public void ResetTest()
        {
            HasBeenUpdated = false;
            HasBeenDrawn = false;
            HasPreparedDraw = false;
        }

        protected override void Update(IGameState state)
        {
            HasBeenUpdated = true;
        }

        protected override void Draw(SpriteBatch spriteBatch)
        {
            HasBeenDrawn = true;
        }

        protected override void PreDraw(SpriteBatch spriteBatch)
        {
            HasPreparedDraw = true;
        }
    }
}
