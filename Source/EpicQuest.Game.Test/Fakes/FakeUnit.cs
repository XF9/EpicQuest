﻿using EpicQuest.Game.Model;

namespace EpicQuest.Game.Test.Fakes
{
    public class FakeUnit : Unit
    {
        public FakeUnit(float evasionrate)
        {
            EvasionRate = evasionrate;
        }

        public override string GetUnitImage()
        {
            return string.Empty;
        }
    }
}
