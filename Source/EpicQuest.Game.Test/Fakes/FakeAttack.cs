﻿using EpicQuest.Game.Model.Attacks;

namespace EpicQuest.Game.Test.Fakes
{
    class FakeAttack : Attack
    {
        public FakeAttack(int mindamage, int maxdamage, float hitchance, float critchance, float critModifier)
        {
            MinDamage = mindamage;
            MaxDamage = maxdamage;
            HitChance = hitchance;
            CritChance = critchance;
            CritModifier = critModifier;
        }
    }
}
