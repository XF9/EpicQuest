﻿using System.Collections.Generic;
using System.Linq;
using EpicQuest.Game.Model;
using EpicQuest.Game.Model.Attacks;
using EpicQuest.Game.Helper;
using EpicQuest.Game.Test.Fakes;
using Machine.Specifications;

namespace EpicQuest.Game.Test.Tests
{
    class CombatCalculatorTests
    {
        [Subject(nameof(CombatCalculator))]
        class when_attacking_with_100_percent_hit_chance
        {
            private static Attack Attack;
            private static Unit Unit;

            private static List<AttackResultDo> AttackResults;

            private Establish context = () =>
            {
                Attack = new FakeAttack(5, 10, 100, 0, 0);
                Unit = new FakeUnit(0);
                AttackResults = new List<AttackResultDo>();
            };

            private Because of = () =>
            {
                for (int i = 0; i < 100; i++)
                    AttackResults.Add(CombatCalculator.CalculateAttackResult(Attack, Unit));
            };

            private It should_not_calculate_lower_than_min_damage = () => AttackResults.All(r => r.Damage >= 5).ShouldBeTrue();
            private It should_not_calculate_higher_than_max_damage = () => AttackResults.All(r => r.Damage <= 10).ShouldBeTrue();
            private It should_have_hit_all = () => AttackResults.Any(r => r.Missed).ShouldBeFalse();
            private It should_have_no_critical_hits = () => AttackResults.Any(r => r.Critical).ShouldBeFalse();
            private It should_have_no_evasions = () => AttackResults.Any(r => r.Evaded).ShouldBeFalse();
        }

        [Subject(nameof(CombatCalculator))]
        class when_attacking_with_100_percent_crit_chance
        {
            private static Attack Attack;
            private static Unit Unit;

            private static List<AttackResultDo> AttackResults;

            private Establish context = () =>
            {
                Attack = new FakeAttack(10, 10, 100, 100, 2);
                Unit = new FakeUnit(0);
                AttackResults = new List<AttackResultDo>();
            };

            private Because of = () =>
            {
                for (int i = 0; i < 100; i++)
                    AttackResults.Add(CombatCalculator.CalculateAttackResult(Attack, Unit));
            };

            private It should_not_calculate_correct_damage = () => AttackResults.All(r => r.Damage == 20).ShouldBeTrue();
            private It should_have_hit_all = () => AttackResults.Any(r => r.Missed).ShouldBeFalse();
            private It should_have_all_critical_hits = () => AttackResults.All(r => r.Critical).ShouldBeTrue();
            private It should_have_no_evasions = () => AttackResults.Any(r => r.Evaded).ShouldBeFalse();
        }

        [Subject(nameof(CombatCalculator))]
        class when_attacking_with_100_percent_evasion_chance
        {
            private static Attack Attack;
            private static Unit Unit;

            private static List<AttackResultDo> AttackResults;

            private Establish context = () =>
            {
                Attack = new FakeAttack(10, 10, 100, 100, 2);
                Unit = new FakeUnit(100);
                AttackResults = new List<AttackResultDo>();
            };

            private Because of = () =>
            {
                for (int i = 0; i < 100; i++)
                    AttackResults.Add(CombatCalculator.CalculateAttackResult(Attack, Unit));
            };

            private It should_have_all_evasions = () => AttackResults.All(r => r.Evaded).ShouldBeTrue();
        }

        [Subject(nameof(CombatCalculator))]
        class when_attacking_with_0_percent_hit_chance
        {
            private static Attack Attack;
            private static Unit Unit;

            private static List<AttackResultDo> AttackResults;

            private Establish context = () =>
            {
                Attack = new FakeAttack(10, 10, 0, 100, 2);
                Unit = new FakeUnit(0);
                AttackResults = new List<AttackResultDo>();
            };

            private Because of = () =>
            {
                for (int i = 0; i < 100; i++)
                    AttackResults.Add(CombatCalculator.CalculateAttackResult(Attack, Unit));
            };

            private It should_have_all_missed = () => AttackResults.All(r => r.Missed).ShouldBeTrue();
        }
    }
}
